<?php

class prestashopCtrl extends standardController {

	function __construct() {

		$this->bridge = new prestashop_bridge();	

	}

	function ApiIndex() {
		echo '<pre>' . print_r($this, 1);
		return $this;
		#return 'Hello World.';

	}

	function getProvincia($id_state) {

		//echo 'dentro getProvincia con id_state = '.$id_state;
		//die();

		$query = "SELECT iso_code FROM `ps_state` WHERE id_state = " . $id_state . " LIMIT 1";

		$statement = $this->bridge->layer->prepare($query);
		$statement->execute();
		$prov = $statement->fetchObject();

		if(is_object($prov)){
			return $prov->iso_code;
		}else{
			return "LT";
		}

		// print_r($prov);
		// die();
		// return $prov;

	}

	function getCurrentstates() {

		//echo 'dentro getProvincia con id_state = '.$id_state;
		//die();

		$query = "SELECT * FROM `ps_order_state_lang` WHERE id_lang = 6 ORDER BY id_order_state ASC";

		$statement = $this->bridge->layer->prepare($query);
		$alls = $statement->execute();
		$allstates = $statement->fetchAll();

		// print_r($allstates);
		// die();

		return $allstates;
		

		// print_r($prov);
		// die();
		// return $prov;

	}

	function CtrlSetstatoordine() {

		$id_order = $_REQUEST['id_order'];
		$current_state = $_REQUEST['current_state'];

		// echo 'dentro setStatoordine con id_order = '.$id_order.' e current_state:'.$current_state;
		// die();

		$query = "UPDATE `ps_orders` SET `current_state`= ".$current_state." WHERE `id_order` = ".$id_order." ";

		$statement = $this->bridge->layer->prepare($query);
		$ordmod = $statement->execute();
		//$ordermod = $statement->fetchObject();

		if($ordmod){
			header('Location: /ordini/dettaglio/id_order:'.$id_order);
		}else{
			echo 'Errore nel Cambio Stato Ordine';
			die();
		}

		
		

		// print_r($prov);
		// die();
		// return $prov;

	}


	function setstatord($id,$state) {

		$id_order = $id;
		$current_state = $state;

		// echo 'dentro setStatoordine con id_order = '.$id_order.' e current_state:'.$current_state;
		// die();

		$query = "UPDATE `ps_orders` SET `current_state`= ".$current_state." WHERE `id_order` = ".$id_order." ";

		$statement = $this->bridge->layer->prepare($query);
		$ordmod = $statement->execute();
		//$ordermod = $statement->fetchObject();

		if($ordmod){
			header('Location: /ordini/dettaglio/id_order:'.$id_order);
		}else{
			echo 'Errore nel Cambio Stato Ordine';
			die();
		}

		
		

		// print_r($prov);
		// die();
		// return $prov;

	}

	function CtrlSetstatomultipli() {

		// echo 'dentro setStatomultipli con order_ids = <br>';
		// print_r($_REQUEST["order_ids"]);
		// die();

		$order_ids = $_REQUEST["order_ids"];

		foreach ($order_ids as $id_order) {
			$query = "UPDATE `ps_orders` SET `current_state`= 3 WHERE `id_order` = ".$id_order." ";

			$statement = $this->bridge->layer->prepare($query);
			$ordmod = $statement->execute();
			//$ordermod = $statement->fetchObject();

			if($ordmod){
				
			}else{
				echo 'Errore nel Cambio Stato Ordine';
				die();
			}
		}

		header('Location: /ordini/index');

		

	}

	function Invoke($model, $action, $args) {

		$argomenti = $_REQUEST; 

		if(!is_array($args))
			$argomenti[] = $args;

		$model_name = 'prestashop_models_' . $model;
		$action_name = 'Action' . ucfirst($action);

		$model = new $model_name($this->bridge);
		if($model) {

			if(method_exists($model, $action_name)) {

				return call_user_func_array(array($model, $action_name), $argomenti);

				return $model->$action_name($_REQUEST);
			} else {
				return 'No.';
			}

		}


	}

	function ApiOrders() { 

		# http://pcsgest.mndrn.com/api/prestashop.orders?action=all
		# http://pcsgest.mndrn.com/api/prestashop.orders?action=single&id_order=10159

		if(!isset($_REQUEST['action']))
			$_REQUEST['action'] = 'all';

		$res = $this->Invoke('orders', $_REQUEST['action']);
		return $res;
		echo '<pre>' . print_r($this->Invoke('orders', $_REQUEST['action']), 1);
		
	}

	function GetOrders() {

		# http://pcsgest.mndrn.com/api/prestashop.orders?action=all
		# http://pcsgest.mndrn.com/api/prestashop.orders?action=single&id_order=10159

		if(!isset($_REQUEST['action']))
			$_REQUEST['action'] = 'all';

		$res = $this->Invoke('orders', $_REQUEST['action']);
		echo '<pre>' . print_r($res, 1);
		
	}

}