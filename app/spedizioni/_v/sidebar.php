<div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
            <li class="filtro1 active"><a href="#" id="tutti">Tutte</a></li>
            <li class="filtro2 "><a href="#" id="davalidare">Indirizzi da Validare</a></li>
            <li class="filtro3 "><a href="#" id="validati">Indirizzi Validati</a></li>
            <li class="filtro4 "><a href="#" id="inseriti">Inserite su GLS</a></li>
            
          </ul>
          
</div>

<script>
$(document).ready(function(){
    $("#davalidare").click(function(){
        $(".tovalidate").show();
        $(".validated").hide();
        $(".inserted").hide();

        $(".filtro1").removeClass("active");
        $(".filtro2").addClass("active");
        $(".filtro3").removeClass("active");
        $(".filtro4").removeClass("active");
    });
    $("#validati").click(function(){
        $(".tovalidate").hide();
        $(".validated").show();
        $(".inserted").hide();

        $(".filtro1").removeClass("active");
        $(".filtro2").removeClass("active");
        $(".filtro3").addClass("active");
        $(".filtro4").removeClass("active");
    });
    $("#inseriti").click(function(){
        $(".tovalidate").hide();
        $(".validated").hide();
        $(".inserted").show();

        $(".filtro1").removeClass("active");
        $(".filtro2").removeClass("active");
        $(".filtro3").removeClass("active");
        $(".filtro4").addClass("active");
    });
    $("#tutti").click(function(){
        $(".tovalidate").show();
        $(".validated").show();
        $(".inserted").show();

        $(".filtro1").addClass("active");
        $(".filtro2").removeClass("active");
        $(".filtro3").removeClass("active");
        $(".filtro4").removeClass("active");
    });
});
</script>