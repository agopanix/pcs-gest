<!DOCTYPE html>
<html>
<head>
	<title><? echo $app_data->config["seo"]["title"]; ?></title>
	<meta name="description" content="<? echo $app_data->config["seo"]["description"]; ?>">
	<link rel="stylesheet" href="<? echo $app_data->___css(); ?>" />
	<script src="<? echo $app_data->___javascripts(); ?>"></script>
</head>
<body>