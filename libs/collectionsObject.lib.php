<?php

/**
 * Gyural > collectionObject
 * Manipolate group of standardObjects!
 *
 * @version 1.10
 * @author Federico Quagliotto <f.quagliotto@mandarinoadv.com>
 */


class collectionsObject {

	/**
	 * Collection of results
	 * 
	 * @var array
	 */
	var $results;

	
	/**
	 * Number of results
	 * 
	 * @return integer
	 */
	public function length() {
		return count($this->results);
	}

	/**
	 * Get the object with the maximum $key field value
	 * 
	 * @param  string $key
	 * @param  boolean $onlyVar if true, only the value will be returned
	 * @return mixed
	 */
	public function max($key, $onlyVar = false) {

		$max = 0;

		if($this->length() > 1) {
			foreach($this->results as $k => $r) {
				if(!isset($r->$key))
					continue;
				if($r->$key > $max) {
					$max = $r->$key;
					$obj = $r;
				}
			}
		}

		if($onlyVar)
			return $max;
		else
			return $obj;

	}

	/**
	 * Get the object with the minimum $key field value
	 * 
	 * @param  string $key
	 * @param  boolean $onlyVar if true, only the value will be returned
	 * @return mixed
	 */
	public function min($key, $onlyVar = false) {


		if($this->length() > 1) {
			foreach($this->results as $k => $r) {
				if(!isset($r->$key))
					continue;
				if($r->$key < $min || !isset($min)) {
					$min = $r->$key;
					$obj = $r;
				}
			}
		}

		if($onlyVar)
			return $min;
		else
			return $obj;

	}

	/**
	 * Get the object with the average $key field value
	 * 
	 * @param  string $key
	 * @param  boolean $onlyVar if true, only the value will be returned
	 * @return mixed
	 */
	public function average($key, $onlyVar = false) {

		$info = $this->filterBy($key);

		$average = ceil($info->length()/2);

		if(!$onlyVar)
			return $info->results[$average];
		else
			return $info->results[$average]->$key;

	}

	/**
	 * Populate the Collection
	 * 
	 * @param  mixed $results Array of object to populate the results
	 * @return mixed
	 */
	public function popolate($results) {
		if(is_array($results))
			$this->results = $results;
		else
			$this->results[] = $results;
		return $this;
	}

	/**
	 * Filter Collection by a $field
	 * 
	 * @param  string $field
	 * @param  string $ord
	 * @return array
	 */
	public function filterBy($field, $ord = ASC) {

		$key = array();
		$output = array();

		if($this->length() == 1)
			return $this;

		foreach($this->results as $index => $row) {
			if(!isset($row->$field))
				continue;
			$key[$index] = $row->$field;
		}

		if($ord == ASC)
			asort($key);
		else if($ord == DESC)
			arsort($key);

		foreach($key as $index => $useless)
			$output[] = $this->results[$index];

		$this->results = $output;

		return $this;

	}

	/**
	 * Change the key of results by $what field
	 * 
	 * @param  string $what
	 * @return array
	 */
	public function changeKey($what) {

		$output = array();

		foreach($this->results as $index => $obj) {
			if(!isset($obj->$what))
				continue;
			if(!isset($output[$obj->$what]))
				$output[$obj->$what] = $obj;
			else {
				if(is_array($output[$obj->$what])) {
					$output[$obj->$what][] = $obj;
				} else {
					$tmp = $output[$obj->$what];
					$output[$obj->$what] = array($tmp, $obj);
				}
			}
		}

		$this->results = $output;

		return $this;

	}

	/**
	 * Link a $key (field) to an other $class object ($class->get($key value))
	 * 
	 * @param  string $key
	 * @param  string $class
	 * @return object
	 */
	public function link($key, $class) {

		foreach($this->results as $index => $obj) {

			if(isset($obj->$key)) {
				$obj->{ 'gyu_' . $key } = LoadClass($class, 1)->get($obj->$key);
			}
		}

		return $this;

	}

	/**
	 * Find an object (1+) into collection
	 * 
	 * @param  string $what
	 * @return mixed
	 */
	public function find($what) {

		if(is_string($what)) {
			preg_match_all('/(\w+)(!=|=|<=|<|>|>=|~=)(\w+)/i', $what, $matches);
			foreach($matches[1] as $k=>$v) {
				$conditions[] = array($v, $matches[2][$k], $matches[3][$k]);
			}
		} else {
			$conditions = $what;
		}

		$output = array();

		foreach($this->results as $k => $value) {

			$canCheck = true;

			foreach($conditions as $checker) {

				if(!$canCheck)
					continue;

				$canCheck = false;

				$value_condition = $checker[2];
				$value_operator = $checker[1];
				$value_toCheck = $value->$checker[0];

				if($value_operator == '=') {
					if(strtolower($value_condition) == strtolower($value_toCheck)) {
						$canCheck = true;
					}
				} else if($value_operator == '!=') {
					if($value_condition != $value_toCheck) {
						$canCheck = true;
					}
				} else if($value_operator == '>') {
					if($value_toCheck > $value_condition) {
						$canCheck = true;
					}
				} else if($value_operator == '<') {
					if($value_toCheck < $value_condition) {
						$canCheck = true;
					}
				} else if($value_operator == '>=') {
					if($value_toCheck >= $value_condition) {
						$canCheck = true;
					}
				} else if($value_operator == '<=') {
					if($value_toCheck <= $value_condition) {
						$canCheck = true;
					}
				} else if($value_operator == '~=') {

					if(stristr($value_toCheck, $value_condition)) {
						$canCheck = true;
					}
				}

			}

			if($canCheck) {
				$output[$k] = $value;
			}

		}

		if(count($output) > 0)
			$this->results = $output;

		return $this;

	}

	/**
	 * Process the whole collection with a specific Method
	 * ($methodName must be a valid Method for the object Class)
	 * 
	 * @param  string $methodName
	 * @return array
	 */
	function map($methodName) {

		foreach($this->results as $obj) {
			if(method_exists($obj, $methodName)) {
				$obj->$methodName();
			}
		}

		return $this;

	}

}
