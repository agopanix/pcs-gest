<?php 
if(Logged()){
?>



    

    <div class="container-fluid">
      <div class="row">
        
        <?php 
            include 'sidebar.php';
        ?>

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <h1 class="page-header">Dashboard Spedizioni Evase</h1>


          <h2 class="sub-header">Ordini in Consegna</h2>
          <?php 
          //print_r($app_data->last10ordini);
          ?>
          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>#ID Ordine</th>
                  <th>Indirizzo Spedizione</th>
                  <th>Stato Ordine</th>
                  <th>Data Ordine</th>
                  <th>Azione</th>
                </tr>
              </thead>
              <tbody>
              <?php 

              foreach ($app_data->last10spedizioni as $ordine) {
                  //   echo '<pre>';
                  // print_r($ordine->obj["shipping_address"]); 
                  //   echo '</pre>';
                  $trclass = "";
                
                      $stringbutton = '<td><a href="/spedizioni/dettaglio/id_order:'.$ordine->id_order.'"><button type="button" class="btn btn-danger">Stampa etichetta</button></a></td>';
                    
                      $parcel = FetchObject(Database()->query("SELECT * FROM `added_parcel` WHERE id_order = '".$ordine->id_order."' ORDER BY id DESC LIMIT 1"),0);

                      if(strlen($parcel->id_order)>0){
                        echo '<tr class="'.$trclass.'">';
                          echo '<td>'.$ordine->id_order.'<br>('.$ordine->reference.')</td>';
                          echo '<td>';
                              //echo 'Alias: '.$ordine->obj["shipping_address"]->alias.'<br>';
                              
                              
                              echo ''.$parcel->DenominazioneDestinatario.'<br>';
                              echo ''.$parcel->IndirizzoDestinatario.'<br>';
                              echo ''.$ordine->obj["shipping_address"]->postcode.', ';
                              echo ''.$parcel->CittaDestinatario.'';
                              // echo 'Telefono: '.$ordine->obj["shipping_address"]->phone.'<br>';
                              // echo 'Cellulare: '.$ordine->obj["shipping_address"]->phone_mobile.'<br>';
                              // echo 'P.Iva: '.$ordine->obj["shipping_address"]->vat_number.'<br>';
                              // echo 'Cod.Fiscale: '.$ordine->obj["shipping_address"]->dni.'<br>';
                              // echo 'Tip. Cliente: '.$ordine->obj["shipping_address"]->custom_tipologiaCliente.'<br>';

                          echo '</td>';
                          
                              echo '<td style="color:green;">Ordine in Consegna</td>';
                          
                          
                          echo '<td>'.$ordine->date_add.'</td>';
                          
                          // stampo il pulsante di AZIONE
                          echo $stringbutton;

                        echo '</tr>';
                      }else{
                          echo '<tr class="'.$trclass.'">';
                            echo '<td>'.$ordine->id_order.'<br>('.$ordine->reference.')</td>';
                            echo '<td>';
                                //echo 'Alias: '.$ordine->obj["shipping_address"]->alias.'<br>';
                                if(strlen($ordine->obj["shipping_address"]->company)>0){
                                    echo ''.$ordine->obj["shipping_address"]->company.', ';    
                                }
                                
                                echo ''.$ordine->obj["shipping_address"]->firstname.' '.$ordine->obj["shipping_address"]->lastname.'<br>';
                                echo ''.$ordine->obj["shipping_address"]->address1.' '.$ordine->obj["shipping_address"]->address2.'<br>';
                                echo ''.$ordine->obj["shipping_address"]->postcode.', ';
                                echo ''.$ordine->obj["shipping_address"]->city.'';
                                // echo 'Telefono: '.$ordine->obj["shipping_address"]->phone.'<br>';
                                // echo 'Cellulare: '.$ordine->obj["shipping_address"]->phone_mobile.'<br>';
                                // echo 'P.Iva: '.$ordine->obj["shipping_address"]->vat_number.'<br>';
                                // echo 'Cod.Fiscale: '.$ordine->obj["shipping_address"]->dni.'<br>';
                                // echo 'Tip. Cliente: '.$ordine->obj["shipping_address"]->custom_tipologiaCliente.'<br>';

                            echo '</td>';
                            echo '<td style="color:green;">Ordine in Consegna</td>';
                            
                            echo '<td>'.$ordine->date_add.'</td>';
                            
                            // stampo il pulsante di AZIONE
                            echo $stringbutton;

                          echo '</tr>';
                      }
    
                  
              } 
              ?> 
                
                
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>

<?php 
}else{
  echo '<h3>Per procedere è necessario effettuare un nuovo <a href="/"><u>LOGIN</u></a></h3>';
}
?>