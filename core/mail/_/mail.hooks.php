<?php

/**
 * Mail Hooks
 *
 * @version 1.10
 * @author Federico Quagliotto <f.quagliotto@mandarinoadv.com>
 */

Functions('mail');
\Gyu\Hooks::set('gyu.engine.off', 'mail__queue');