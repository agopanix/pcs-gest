<?php 
if(Logged()){
?>



    

    <div class="container-fluid">
      <div class="row">
        
        <?php 
            include 'sidebar.php';
        ?>

         <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <h1 class="page-header">Dashboard Ordini</h1>

         <!--  <div class="row placeholders">
            <div class="col-xs-6 col-sm-3 placeholder">
              <img src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" width="200" height="200" class="img-responsive" alt="Generic placeholder thumbnail">
              <h4>Grafico 1</h4>
              <span class="text-muted">Decidiamo cosa mettere</span>
            </div>
            <div class="col-xs-6 col-sm-3 placeholder">
              <img src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" width="200" height="200" class="img-responsive" alt="Generic placeholder thumbnail">
              <h4>Grafico 2</h4>
              <span class="text-muted">Decidiamo cosa mettere</span>
            </div>
            <div class="col-xs-6 col-sm-3 placeholder">
              <img src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" width="200" height="200" class="img-responsive" alt="Generic placeholder thumbnail">
              <h4>Grafico 3</h4>
              <span class="text-muted">Decidiamo cosa mettere</span>
            </div>
            <div class="col-xs-6 col-sm-3 placeholder">
              <img src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" width="200" height="200" class="img-responsive" alt="Generic placeholder thumbnail">
              <h4>Grafico 4</h4>
              <span class="text-muted">Decidiamo cosa mettere</span>
            </div>
          </div> -->

          <h2 class="sub-header">Ultimi 1000 ordini pervenuti</h2>
          <?php 
          //print_r($app_data->last10ordini);
          ?>
          <form method="POST" action="/prestashop/setstatomultipli">
              <div style="clear:both; height:5px;"></div>
                  <button type="submit" class="btn btn-success">Sblocca per Spedizione</button>
               <div style="clear:both; height:5px;"></div>

          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>Seleziona</th>
                  <th>#ID Ordine</th>
                  <th>Indirizzo Spedizione</th>
                  <th>Tipo Pagamento</th>
                  <th>Totale ordine</th>
                  <th>Data Ordine</th>
                  <th>Azione</th>
                </tr>
              </thead>
              <tbody>
              
              <?php 
              foreach ($app_data->last10ordini as $ordine) {
                  //   echo '<pre>';
                  // print_r($ordine->obj["shipping_address"]); 
                  //   echo '</pre>';

                if($ordine->current_state == 2){

                  $trclass = "class=\"accettato\"";

                }elseif($ordine->current_state == 14){

                  $trclass = "class=\"lavorazione\"";

                }elseif($ordine->current_state == 3){

                  $trclass = "class=\"preparazione\"";

                }else{
                  $trclass = "class=\"altro\"";
                }

                  echo '<tr '.$trclass.'>';

                    if($ordine->current_state != 3 && $ordine->current_state != 4 && $ordine->current_state != 5 && $ordine->current_state != 6 
                      && $ordine->current_state != 7 && $ordine->current_state != 17 && $ordine->current_state != 21){
                      $checkdistato = "<input type=\"checkbox\" name=\"order_ids[]\" value=\"".$ordine->id_order."\"";
                    }else{
                      $checkdistato = "&nbsp;";
                    }
                    echo '<td>'.$checkdistato.'</td>';
                    
                    echo '<td>'.$ordine->id_order.'<br>('.$ordine->reference.')</td>';
                    echo '<td>';
                        //echo 'Alias: '.$ordine->obj["shipping_address"]->alias.'<br>';
                        if(strlen($ordine->obj["shipping_address"]->company)>0){
                            echo ''.$ordine->obj["shipping_address"]->company.', ';    
                        }
                        
                        echo ''.$ordine->obj["shipping_address"]->firstname.' '.$ordine->obj["shipping_address"]->lastname.'<br>';
                        echo ''.$ordine->obj["shipping_address"]->address1.' '.$ordine->obj["shipping_address"]->address2.'<br>';
                        echo ''.$ordine->obj["shipping_address"]->postcode.', ';
                        echo ''.$ordine->obj["shipping_address"]->city.'';
                        // echo 'Telefono: '.$ordine->obj["shipping_address"]->phone.'<br>';
                        // echo 'Cellulare: '.$ordine->obj["shipping_address"]->phone_mobile.'<br>';
                        // echo 'P.Iva: '.$ordine->obj["shipping_address"]->vat_number.'<br>';
                        // echo 'Cod.Fiscale: '.$ordine->obj["shipping_address"]->dni.'<br>';
                        // echo 'Tip. Cliente: '.$ordine->obj["shipping_address"]->custom_tipologiaCliente.'<br>';

                    echo '</td>';
                    echo '<td>'.$ordine->payment.'</td>';
                    echo '<td>'.number_format($ordine->total_paid_tax_incl,2,".","").' &euro;</td>';
                    echo '<td>'.$ordine->date_add.'</td>';
                    echo '<td><a href="/ordini/dettaglio/id_order:'.$ordine->id_order.'"><button type="button" class="btn btn-success">Dettagli</button></a></td>';
                  echo '</tr>';
              } 
              ?> 
                  
              </tbody>
            </table>
          </div>

          <div style="clear:both; height:5px;"></div>
          <button type="submit" class="btn btn-success">Sblocca per Spedizione</button>

        </form>  

        </div>
      </div>
    </div>

<?php 
}else{
  echo '<h3>Per procedere è necessario effettuare un nuovo <a href="/"><u>LOGIN</u></a></h3>';
}
?>