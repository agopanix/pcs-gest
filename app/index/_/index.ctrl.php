<?php


class indexCtrl extends standardController {

	var $index_tollerant = false;

	function __construct() {
		$this->index = LoadClass('index', 1); 
	}
	
	function CtrlIndex() {  
		$this->index->head();
		Application('index/_v/home', null, $this);
		$this->index->foot();
	}

}

