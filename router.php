<?php

/**
 * Gyural > Router
 *
 * @author Federico Quagliotto <f.quagliotto@mandarinoadv.com>
 * @version 1.10
 */

$router = LoadClass('router', 1);

\Gyu\Hooks::get('gyu.router.prepend', $router);

$router->map('is_gyural', function() {
	echo 'yes it is.';
	echo 'v. '. version . '<br /><br />';
	echo '<a href="http://www.mandarinoadv.com">http://www.mandarinoadv.com</a><br />';
	echo '<a href="http://www.gyural.com">http://www.gyural.com</a>';
});

\Gyu\Hooks::get('gyu.router.append', $router);