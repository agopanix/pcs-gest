<?php

class prestashop_models_orders extends prestashop_model {

	function shippingAddress($order) {

		$vars = [':id' => $order->id_address_delivery];

		$query = "SELECT * FROM `ps_address` WHERE id_address = :id LIMIT 1";

		$statement = $this->bridge->layer->prepare($query);
		
		$statement->execute($vars);
		$row = $statement->fetchObject();

		if(is_object($row))
			$row->custom_tipologiaCliente = ($tmp_tipo = $this->getTipologiaCliente($row->id_customer, $row->id_address)) ? $tmp_tipo->tipologia : false;
		
		return $row;

	}

	function products($order) {

		$query = "SELECT * FROM `ps_order_detail` WHERE id_order = :id";

		$statement = $this->bridge->layer->prepare($query);
		$statement->setFetchMode(PDO::FETCH_OBJ);
		$statement->execute([':id' => $order->id_order]);
		$rows = $statement->fetchAll();
		
		return $rows;

	}

	function getOrder($id_order) {

		$order_query = "SELECT * FROM `ps_orders` WHERE id_order = " . $id_order . " LIMIT 1";

		$statement = $this->bridge->layer->prepare($order_query);
		$statement->execute();
		$order = $statement->fetchObject();

		if(is_object($order)) {
			$order->obj = [
				'shipping_address' => $this->shippingAddress($order),
				'products' => $this->products($order)
			];
		}

		return $order;

	}


	function getProvincia($id_state) {

		$query = "SELECT name FROM `ps_state` WHERE id_state = " . $id_state . " LIMIT 1";

		$statement = $this->bridge->layer->prepare($query);
		$statement->execute();
		$prov = $statement->fetchObject();
		return $prov;

	}

	function getTipologiaCliente($id_customer, $id_address = null) {

		$query = "SELECT * FROM `ps_tipologiacliente` WHERE id_customer = " . $id_customer . " AND id_address = " . $id_address . " LIMIT 1";

		$statement = $this->bridge->layer->prepare($query);
		$statement->execute();
		$ps_tipologiacliente = $statement->fetchObject();
		return $ps_tipologiacliente;

	}

	# http://pcsgest.mndrn.com/api/prestashop.orders?action=all
	function ActionAll() {

		$vars = [
			':limitStart' => 0, 
			':limitEnd' => 1000,
			':order' => 'DESC'
		];

		$query = str_replace(array_keys($vars), $vars, "SELECT id_order FROM `ps_orders` ORDER BY `ps_orders`.`id_order` :order LIMIT :limitStart, :limitEnd");

		return $this->Collection($query);

	}

	function ActionSped() {

		$vars = [
			':limitStart' => 0, 
			':limitEnd' => 1000,
			':order' => 'ASC'
		];

		$query = str_replace(array_keys($vars), $vars, "SELECT id_order FROM `ps_orders` WHERE `current_state` = 3 ORDER BY `ps_orders`.`id_order` :order LIMIT :limitStart, :limitEnd");

		return $this->Collection($query);

	}

	function ActionEvase() {

		$vars = [
			':limitStart' => 0, 
			':limitEnd' => 1000,
			':order' => 'ASC'
		];

		$query = str_replace(array_keys($vars), $vars, "SELECT id_order FROM `ps_orders` WHERE `current_state` = 4 ORDER BY `ps_orders`.`id_order` :order LIMIT :limitStart, :limitEnd");

		return $this->Collection($query);

	}

	// Il primo argomento è sempre $request, gli altri invece li scegli te
	function ActionDetail($request, $id_ordine) {

		$vars = [
			':id_order' => $id_ordine,
			':limitStart' => 0, 
			':limitEnd' => 1,
			':order' => 'DESC'
		];

		$query = str_replace(array_keys($vars), $vars, "SELECT id_order FROM `ps_orders` ORDER BY `ps_orders`.`id_order` :order LIMIT :limitStart, :limitEnd");

		return $this->Collection($query);

	}

	function Collection($query) {

		$statement = $this->bridge->layer->prepare($query);
		$statement->setFetchMode(PDO::FETCH_ASSOC);
		
		$statement->execute();
		$rows = $statement->fetchAll();
		
		foreach($rows as $single_order)
			$orders[$single_order['id_order']] = $this->getOrder($single_order['id_order']);

		return $orders;

	}

	# http://pcsgest.mndrn.com/api/prestashop.orders?action=single&id_order=10159
	function ActionSingle() {

		#$query = "59986";
		# 10159
		return $this->getOrder($_REQUEST['id_order']);

	}

	/*# http://pcsgest.mndrn.com/api/prestashop.orders?action=single&id_order=10159
	function ActionSingle($request, $id) {

		#$query = "59986";
		# 10159
		return $this->getOrder($id);

	}*/

}