<?php

/**
 * Gyural > Funcs > Debug
 *
 * @version 1.10
 * @author Federico Quagliotto <f.quagliotto@mandarinoadv.com>
 */

$GLOBALS["__sdkLogged"] = time().md5(rand());

/**
 * Save a log
 * 
 * @param  string $message
 * @param  integer $type
 * @return false
 */
function deb_log($message, $type) {

	global $env;

	$th = array(time(), $message, $type);
	$GLOBALS["__stack"][] = $th;
	$file = cache . 'sys/test/log-' . $GLOBALS["__sdkLogged"];
	if(dev && logStack)
		file_put_contents($file, json_encode($th) . "\n", FILE_APPEND);

}

/**
 * Trig an Error.
 * if $die == 1 it will also terminate the execution.
 * 
 * @param  string $message
 * @param  integer $die
 * @return false
 */
function deb_error($message, $die = 0) {

	$env = $GLOBALS["env"];

	$info = debug_backtrace();
	
	$env->error = true;
	$env->errorMessages[] = array('message' => $message, 'debug' => array('file' => $info[0]["file"], 'line' => $info[0]["line"], 'info' => $info[0], 'info2' => $info));

	deb_log($message, 'error');

	if($die == 1) {
		$env->errorStop = 1;
		$error_id = uniqid();
		if(dev == 1) {
			deb_register($error_id, '!!DEV ENABLED!! ' . $message);
			\Gyu\Hooks::get('gyu.off', time());
			\Gyu\Hooks::get('gyu.dev-zone', time(), $env);
		} else {
			HttpResponse(500);
			deb_register($error_id, $message);
			die('<h2>Internal Error</h2><p>Error: ' . $error_id . '</p><p><hr /><em>'.$message.'</em></p>');
		}
		die();
	}
}

/**
 * Save a critical error to file
 * 
 * @param  integer $errorId
 * @param  string $message
 * @return false
 */
function deb_register($errorId, $message) {
	$file = cache . 'sys/error.log';
	$pre = file_get_contents($file);
	file_put_contents($file, "[".date('r')."]\t" . $errorId . "\t" . $message . "\n" . $pre); 
}

/**
 * All deb_log events
 * 
 * @return array
 */
function deb_logs() {

	global $env;
	return $GLOBALS["__stack"];

}

/**
 * Destroy the deb_log file
 * 
 * @return false
 */
function deb_clean() {

	$file = cache . 'sys/test/log-' . $GLOBALS["__sdkLogged"];
	if(is_file($file)) {
		unlink($file);
	}

}

/**
 * List of installed Underscore Apps
 * 
 * @return array
 */
function deb_installed_app() {
	
	if ($handle = opendir(application)) {
		while (false !== ($entry = readdir($handle))) {
			if ($entry != "." && $entry != "..") {
				if(isApplication($entry) && is_dir(application . $entry)) {
					$object = null;
					$object["name"] = $entry;
					$object["detail"] = ApplicationDetail($entry);
					$installedApp[$object["name"]] = $object;
				}
			}
		}
		closedir($handle);
	}

	if(legacy == 0) {

		if($handle = opendir(applicationCore)) {
			while (false !== ($entry = readdir($handle))) {
				if ($entry != "." && $entry != "..") {
					if(isApplication($entry) && is_dir(applicationCore . $entry)) {
						$object = null;
						$object["name"] = $entry;
						$object["detail"] = ApplicationDetail($entry);
						$object["isCore"] = true;
						if(!isset($installedApp[$object["name"]]))
							$installedApp[$object["name"]] = $object;
						else
							$installedApp[$object["name"]]["alsoCore"] = true;
					}
				}
			}
			
			closedir($handle);

		}

	}

	return $installedApp;
	
}