<?php

/*

------------
Gyural 1.9.1
------------

Filename: /libs/gyural.lib.php
 Version: 1.9.1
  Author: federicoq <f.quagliotto@mandarinoadv.com>
    Date: 04/11/2014

----
Desc
----

Gyural..

*/

class gyural {

	var $creationTime = null;
	var $userSession = null;
	var $methods = array();

	function __construct($methods) {
		$this->url = rtrim($_SERVER["REQUEST_URI"], '/');
		$this->creationTime = time();
		$this->userSession = Me();
		$this->methods = $methods;
	}

	function debug() {
		echo '<pre>';
		print_r($this);
		echo '</pre>';
	}

	function controllerExists($controllerName) {

		return(LoadController($controllerName));

	}

	function Controller($controllerName) {
		return LoadController($controllerName, 1);
	}

	function IsCallable($args, $methods) {

		if($methods["app"] == true) {
			if(isset($args["App"])) {
				$controller = $args["App"];
				$type = 'App';
			}
		} else if($methods["ajax"] == true) {
			if(isset($args["Ajax"])) {
				$type = 'Ajax';
				$controller = $args["Ajax"];
			}
		} else if($methods["get"] == true) {
			if(isset($args["Get"])) {
				$type = "Get";
				$controller = $args["Get"];
			}
		} else if($methods["post"] == true) {
			if(isset($args["Post"])) {
				$controller = $args["Post"];
				$type = "Post";
			}
		}

		if(!isset($controller)) {
			if(isset($args["Ctrl"])) {
				$controller = $args["Ctrl"];
				$type = 'Ctrl';
			}
		}

		if(!$controller)
			return false;

		return $controller;

	}

	function exec() {

		$controllers = false;
		$standard = false;

		if(isset($this->internal_execution)) {
			$func = $this->candidate;
			return $func();
		}

		if($this->availableControllers) {

			list($Controller, $Method) = explode('/', $this->availableControllers);

			$clone = clone $this;

			$clone->execution = LoadController($Controller, 1);
			$clone->execution->gyu_environment = $this;

			return $clone->execution->__exec($Method, @$type);

		} else if(strlen($this->availableApplication) > 0) {

			if(isset($this->app_data))
				$app_data = $this->app_data;

			include application . $this->availableApplication;

		} else {

			if($this->availableControllersError) {

				list($Controller, $Method) = explode('/', $this->availableControllersError);

				$clone = clone $this;

				$clone->execution = LoadController($Controller, 1);
				$clone->execution->gyu_environment = $this;

				HttpResponse(404);

				return $clone->execution->__exec($Method, $type);

			} else if(strlen($this->availableApplicationError) > 0) {

				if(isset($this->app_data))
					$app_data = $this->app_data;

				HttpResponse(404);
				include application . $this->availableApplicationError;

			}

		}

	}

}

?>