<?php

/**
 * Gyural > Hooks Funcs
 * see /vendors/Gyu/Hooks.php
 * this file is for retrocompatibility only.
 *
 * @version 1.10
 * @author Federico Quagliotto <f.quagliotto@mandarinoadv.com>
 */

function hooks__get($event) {
	call_user_func_array('\Gyu\Hooks::get', func_get_args());
}

function hooks__set($event, $function, $priority = 0) {
	\Gyu\Hooks::set($event, $function, $priority);
}

function hooks__load($attr = null) {
	\Gyu\Hooks::load($attr);
}

function hooks__rebuilt() {
	\Gyu\Hooks::rebuilt();
}

function hooks__empty() {
	\Gyu\Hooks::trash();
}