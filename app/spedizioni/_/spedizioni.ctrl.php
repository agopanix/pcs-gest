<?php


class spedizioniCtrl extends standardController {

	var $index_tollerant = false;

	function __construct() {
		$this->spedizioni = LoadClass('spedizioni', 1);
		$this->ordini = LoadClass('ordini', 1);
		$this->index = LoadClass('index', 1); 
	}
	
	function CtrlIndex() {  
		$this->last10spedizioni = LoadApp('prestashop', 1)->Invoke('orders', 'sped');
		$this->index->headlogged();
		Application('spedizioni/_v/index', null, $this);
		$this->index->foot();
	}

	function CtrlEvase() {  
		$this->last10spedizioni = LoadApp('prestashop', 1)->Invoke('orders', 'evase');
		$this->index->headlogged();
		Application('spedizioni/_v/evase', null, $this);
		$this->index->foot();
	}

	function CtrlDettaglio() {  
		$this->ordinedettaglio = LoadApp('prestashop', 1)->Invoke('orders', 'single');
		$this->index->headlogged();
		Application('spedizioni/_v/detail', null, $this);
		$this->index->foot();
	}

	function CtrlStampa() {  
		$id_order = $_REQUEST["id_order"];
		$parcel = FetchObject(Database()->query("SELECT * FROM `added_parcel` WHERE id_order = '".$id_order."' ORDER BY id DESC LIMIT 1"),0);
		$this->indirizzo = $parcel;
		Application('spedizioni/_v/stampaetichetta', null, $this);
		//$this->index->foot();
	}

	function CtrlGetValidato($id_order) {  
		$valaddress = FetchObject(Database()->query("SELECT * FROM `validated_addresses` WHERE id_order = '".$id_order."' ORDER BY id DESC LIMIT 1"),0);
		return $valaddress;
	}

}

?>