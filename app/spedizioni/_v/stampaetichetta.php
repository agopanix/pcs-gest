<?php
require('/var/www/pcsgest.mndrn.com/fpdf/fpdf.php');
define('EURO', chr(128));
$larghezza = "10.5";
$altezza = "10.5";
$formato = array($larghezza, $altezza);
$pdf_doc = new FPDF('P', 'cm'); 
$pdf_doc->SetFillColor(136, 196, 150);
$pdf_doc->AddPage();
$pdf_doc->SetFont('Arial','B',12);

$indirizzo = $app_data->indirizzo;





$pdf_doc->SetXY(0,0);
$pdf_doc->SetFont('Arial','B',11);


$pdf_doc->Cell(6,0.7,''.$indirizzo->DenominazioneMittente.'',1,0,L);

$pdf_doc->Cell(2,0.7,'P/V '.$indirizzo->RapportoPesoVolume.'',1,0,L);

$pdf_doc->Cell(2,0.7,''.$indirizzo->DataSpedizione.'',1,0,R);

$y0=$pdf_doc->GetY();
$pdf_doc->SetY($y0+0.1);
$pdf_doc->SetX(0);

$pdf_doc->SetFont('Arial','B',36);

$pdf_doc->Cell(7,2.5,''.$indirizzo->DescrizioneSedeDestino.'',0,0,L);
$pdf_doc->Cell(3,2.5,''.$indirizzo->SiglaCSM.'',0,0,R);

// $y0=$pdf_doc->GetY();
// $pdf_doc->SetY($y0+0.1);
// $pdf_doc->SetX(0);

$pdf_doc->SetXY(0,2);

$pdf_doc->SetFont('Arial','B',18);

$pdf_doc->Cell(1,1.2,''.$indirizzo->CodiceZona.'',1,0,L);

$pdf_doc->SetFont('Arial','',10);

$pdf_doc->Cell(7,0.4,''.$indirizzo->DenominazioneDestinatario.'',0,0,L);

$pdf_doc->SetXY(1,2.4);

$pdf_doc->Cell(7,0.4,''.$indirizzo->IndirizzoDestinatario.'',0,0,L);







$prefisso = trim(str_replace(' / ', '-', $indirizzo->id_order));


$pdf_doc->Output('download/'.$prefisso.'.pdf','F');


$pdf_doc->Output();

?>