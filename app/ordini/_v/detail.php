<?php 
if(Logged()){
?>



    

    <div class="container-fluid">
      <div class="row">
        
        <?php 
            //include 'sidebar.php';
        ?>
        
        <div class="col-sm-12 col-md-12 main">
          <h1 class="page-header">Dettaglio Ordine</h1>

          
          <div class="jumbotron">
            <?php 
            // echo '<pre>';
            // print_r($app_data->ordinedettaglio);
            // echo '</pre>';

            $ordine = $app_data->ordinedettaglio;
            
              echo '<h2>Ordine #: '.$ordine->id_order.' ('.$ordine->reference.')</h2>';
               
            
            ?>
            
            
          </div>

          <div class="jumbotron">

            <div class="form-group">
                <label for="sel1">Cambia Stato:</label>
                
                <form method="POST" action="/prestashop/setstatoordine">
                  <select class="form-control" name="current_state">
                      <?php 
                      foreach ($app_data->allstates as $stato) {
                        if($stato["id_order_state"]==$ordine->current_state){
                          $selectedstate = "selected";
                        }else{
                          $selectedstate = "";
                        }
                        echo '<option '.$selectedstate.' value="'.$stato["id_order_state"].'">'.$stato["name"].'</option>';
                      }
                      ?>
                      
                  </select>
                  <input type="hidden" name="id_order" value="<?=$ordine->id_order; ?>"/>
                  <div style="clear:both; height:5px;"></div>
                  <button type="submit" class="btn btn-success">Cambia Stato Ordine</button>

                </form>
              </div>

            <?php 
            echo '<p>';
              echo '<h3>Prodotti associati all\'ordine</h3>';
              // echo '<pre>';
              // print_r($ordine->obj["products"]);
              // echo '</pre>';
              // echo '<pre>';
              // print_r($app_data->allstates);
              // echo '</pre>';

              ?>

              

              <div class="table-responsive">
                <table class="table table-striped">
                  <thead>
                    <tr>
                      <th>#Prod.Ref.</th>
                      <th>Nome Prodotto</th>
                      <th>Prez. Unit.</th>
                      <th>Quantità</th>
                      <th>Prez. Tot.</th>
                    </tr>
                  </thead>
                  <tbody>

             <?php  
              foreach ($ordine->obj["products"] as $prodotto) {
                echo '<tr>';
                    echo '<td>'.$prodotto->product_reference.'</td>';
                    echo '<td>'.$prodotto->product_name.'</td>';
                    echo '<td>'.number_format($prodotto->unit_price_tax_incl,2,".","").' &euro;</td>';
                    echo '<td>'.$prodotto->product_quantity.'</td>';
                    echo '<td>'.number_format($prodotto->total_price_tax_incl,2,".","").' &euro;</td>';
                echo '</tr>';
              }
              ?>

                  </tbody>
                </table>
              </div>

            <?php 
            echo '<h4>Totale Ordine: '.number_format($ordine->total_paid,2,".","").' &euro;</h4>';
            echo '</p>';
            ?>
          </div>

          <div class="jumbotron">
            <?php 
            echo '<p>';
              echo '<h3>Indirizzo di Spedizione</h3>';
              echo 'Alias: '.$ordine->obj["shipping_address"]->alias.'<br>';
              if(strlen($ordine->obj["shipping_address"]->company)>0){
                    echo 'Nome Azienda: '.$ordine->obj["shipping_address"]->company.'<br>';    
              }
                        
              echo 'Nome e Cognome: '.$ordine->obj["shipping_address"]->firstname.' '.$ordine->obj["shipping_address"]->lastname.'<br>';
              echo 'Indirizzo: '.$ordine->obj["shipping_address"]->address1.' '.$ordine->obj["shipping_address"]->address2.'<br>';
              echo 'CAP: '.$ordine->obj["shipping_address"]->postcode.'<br>';
              echo 'Città: '.$ordine->obj["shipping_address"]->city.'<br>';
              if(strlen($ordine->obj["shipping_address"]->phone)>0){
                echo 'Telefono: '.$ordine->obj["shipping_address"]->phone.'<br>';
              }
              if(strlen($ordine->obj["shipping_address"]->phone_mobile)>0){
                echo 'Cellulare: '.$ordine->obj["shipping_address"]->phone_mobile.'<br>';
              }
              if(strlen($ordine->obj["shipping_address"]->vat_number)>0){
                echo 'P.Iva: '.$ordine->obj["shipping_address"]->vat_number.'<br>';
              }
              if(strlen($ordine->obj["shipping_address"]->dni)>0){
                echo 'Cod.Fiscale: '.$ordine->obj["shipping_address"]->dni.'<br>';
              }
              if(strlen($ordine->obj["shipping_address"]->custom_tipologiaCliente)>0){
                echo 'Tip. Cliente: '.$ordine->obj["shipping_address"]->custom_tipologiaCliente.'<br>';
              } 

              echo '</p>';
            ?>
          </div>
          
          
        </div>
      </div>
    </div>

<?php 
}
?>