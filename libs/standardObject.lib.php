<?php

/**
 * Gyural > standardObject
 *
 * @version 1.10
 * @author Federico Quagliotto <f.quagliotto@mandarinoadv.com>
 */

class standardObject {

	/**
	 * PRIMARY KEY
	 * @var string $gyu_id;
	 */
	var $gyu_id = 'id';

	function __construct() {}

	/**
	 * Get Attribute
	 * if exists, execute the get$name method, otherwise return the attribute value
	 * 
	 * @param  string $name The name of the attribute (column name)
	 * @param  mixed $args List of arguments
	 * @return mixed
	 */
	public function getAttr($name, $args = null) {

		if(method_exists($this, 'get' . ucFirst($name)))
			return call_user_func(array($this, 'get' . ucFirst($name)), $args);
		else
			return stripslashes($this->$name);

	}

	/**
	 * Set Attribute
	 * like the getter, set$name is the first try
	 * 
	 * @param string $key The name of the attribute (column name)
	 * @param mixed $vValue The value for the attribute
	 * @return mixed
	 */
	public function setAttr($key, $vValue) {

		if(method_exists($this, 'set' . ucFirst($key)))
			return call_user_func(array($this, 'set' . ucFirst($key)), $vValue);
		else
			return $this->$key = $vValue;

	}

	/**
	 * This works like: getAttr(), render() binded to render$name
	 * 
	 * @param  string $name The name of the attribute (column name)
	 * @param  mixed $args List of Arguments
	 * @return mixed
	 */
	public function verb($name, $args = null) {

		if(method_exists($this, 'verb' . ucFirst($name)))
			return call_user_func(array($this, 'verb' . ucFirst($name)), $args);
		else
			return stripslashes($this->$name);

	}


	/**
	 * This works like: ->getAttr, ->verb binded to render$name
	 * 
	 * @param  string $name The name of the attribute (column name)
	 * @param  mixed $args List of arguments
	 * @return mixed
	 */
	public function render($name, $args) {

		if(method_exists($this, 'render' . ucFirst($name)))
			return call_user_func(array($this, 'render' . ucFirst($name)), $args);
		else
			return stripslashes($this->$name);

	}

	/**
	 * Check if an Attribute is null
	 * 
	 * @param  string $what The name of attribute (column name)
	 * @return boolean
	 */
	public function notNull($what) {

		if(strlen($this->getAttr($what)) > 0 && $this->getAttr($what) != 0)
			return true;
		else
			return false;

	}

	/**
	 * Refill an object with an associative array.
	 * 
	 * @param  mixed $vArray associative array
	 * @return [type]
	 */
	public function refill($vArray = null) {

		$caution = false;
		if(!$vArray)
			return false;

		if(isset($this->gyu_refill))
			$caution = true;

		foreach($vArray as $key=>$vValue) {
			if(!in_array($key, $this->gyu_refill) && $caution == true)
				deb_error($key . ' can\'t be refilled in <em>' . get_class($this) . '</em> class');
			else 
				$this->setAttr($key, $vValue);
		}

		return $this;

	}

	/**
	 * Database Layer (HELPER)
	 * Helper for SELECT queries
	 * conditions/option passed as function arguments
	 * 
	 * @param mixed
	 * @return object
	 */
	public function filter() {

		$args = func_get_args();
		return $this->filterExecute($args);

	}

	/**
	 * Database Layer (HELPER)
	 * Helper for SELECT queries
	 * conditions/option passed as array
	 * 
	 * @param  mixed $args List of arguments
	 * @return [type]
	 */
	public function filter_array($args) {

		return $this->filterExecute($args);

	}

	/**
	 * Database Layer
	 * Helper for SELECT queries
	 * 
	 * @param  mixed $args List of arguments
	 * @return mixed object or array of objects
	 */
	public function filterExecute($args) {

		$whereC = array();
		$endQ = array();
		$isArray = 1;
		$limitIndication = ' LIMIT 0,100';
		$constructArgs = null;
		$className = get_class($this);
		$collection = false;

		foreach($args as $a) {
			$pT = true;

			if(is_array($a)) {
				if(count($a) == 2) {
					list($key, $value) = $a;
					if($key == 'LIMIT') {
						$pT = false;
						$limitIndication = 'LIMIT ' . $a[1];
					} else if($key == '#collection') {
						$pT = false;
						$collection = $value;
					} else {
						$operator = '=';
					}
				} else {
					if($a[0] == 'LIMIT') {
						$pT = false;
						$limitIndication = 'LIMIT ' . $a[1] . ',' . $a[2];
					} else {
						$pT = true;
						list($key, $operator, $value) = $a;
					}
				}

				if($pT) {
					if($operator == 'LIKE')
						$whereC[] = "`".$key."` " . $operator . " '%" . $value . "%'";
					else
						$whereC[] = "`".$key."` " . $operator . " '" . $value . "'";
				}

			} else if(is_object($a)) {
				$constructArgs[] = $a;
			} else {
				if($a == 'ONE') {
					if($limitIndication != 'LIMIT 1') {
						$limitIndication = ' LIMIT 1';
					}
					$isArray = 0;
				}
				else if($a == 'COUNT') {
					$isArray = 3;
				}
				else if($a == '#collection') {
					$collection = 'collectionsObject';
				}
				else
					$endQ[] = $a;
			}
		}

		$label = $whereC ? ' WHERE ' : '';

		if($isArray == 3){
			$query = "SELECT COUNT(*) AS gyu_tot FROM `".$this->gyu_table."` " . $label . implode(' AND ', $whereC) . ' ' . implode(" ", $endQ) . ' ';
			$size = FetchObject(Database()->query($query))->gyu_tot;
			return $size;
		} else {
			$query = "SELECT * FROM `".$this->gyu_table."` " . $label . implode(' AND ', $whereC) . ' ' . implode(" ", $endQ) . ' ' . $limitIndication;
			if(Database())
				$res = FetchObject(Database()->query($query), $isArray, $className, $constructArgs, $query);
			if($collection) {
				return LoadClass($collection, 1)->popolate($res);
			} else
				return $res;
		}

	}

	/**
	 * Database Layer
	 * Get the object identified by the primary_key field
	 * 
	 * @param  mixed $index Value of the primary key to retrive
	 * @param  array $constructArgs List of arguments for __construct of the class
	 * @return [type]
	 */
	public function get($index, $constructArgs = null) {
		return $this->filter(array($this->gyu_id, $index), array('LIMIT', 1), 'ONE', $constructArgs);
	}

	/**
	 * Database Layer
	 * Helper for INSERT queries (NOT EXECUTION)
	 * 
	 * @param  array $vArray List of Attributes (if !null ->refill())
	 * @return string
	 */
	public function hang($vArray = null) {

		if($vArray)
			$this->refill($vArray);

		return CreateQuery('I', $this->gyu_table, $this);

	}

	/**
	 * Database Layer
	 * Helper for INSERT queries (EXECUTION)
	 * 
	 * @param  array $vArray List of Attributes
	 * @return mixed the last-insert id (note that $this also contain the whole object)
	 */
	public function hangExecute($vArray = null) {

		if($query = $this->hang($vArray)) {
			Database()->query($query);
			$this->{$this->gyu_id} = Database()->insert_id;
			return $this->{$this->gyu_id};
		}

	}

	/**
	 * Database Layer
	 * Helper for UPDATE queries (NOT EXECUTION)
	 * 
	 * @param  array $vArray List of Attributes (if !null ->refill())
	 * @param  mixed $id The identifier (if null $this->gyu_id)
	 * @return string
	 */
	public function put($vArray = null, $id = null) {

		if(isset($vArray) && !is_array($vArray))
			$id = $vArray;
		else if(isset($vArray))
			$this->refill($vArray);

		if($id == null)
			$id = $this->gyu_id . ' = ' . $this->getAttr($this->gyu_id);

		return CreateQuery('U', $this->gyu_table, $this, $id);

	}

	/**
	 * Database Layer
	 * Helper for UPDATE queries (EXECUTION)
	 * 
	 * @param  array $vArray List of attributes (if !null ->refill())
	 * @param  mixed $id The identifier (if null $this->gyu_id)
	 * @return resource (note that $this still contain the whole object)
	 */
	public function putExecute($vArray = null, $id = null) {

		if($query = $this->put($vArray, $id))
			return Database()->query($query);

	}

	/**
	 * Database Layer
	 * Helper for DELETE queries (NOT EXECUTION)
	 * 
	 * @param  mixed $id The identifier (if null $this->gyu_id)
	 * @return string
	 */
	public function delete($id = null) {

		if(!$id)
			$id = $this->gyu_id . ' = ' . $this->getAttr($this->gyu_id);

		return "DELETE FROM `" .$this->gyu_table . "` WHERE " . $id;

	}

	/**
	 * Database Layer
	 * Helper for DELETE queries (EXECUTION)
	 * 
	 * @param  mixed $id The identifier
	 * @return resource ($this is still active)
	 */
	public function deleteExecute($id = null) {

		if($query = $this->delete($id))
			return Database()->query($query);

	}

	
	/**
	 * Database Layer, Relation Manager
	 * $this to One.
	 * ->ROneObjectName
	 * 
	 * @param  string $name Name of object
	 * @param  mixed $var Configuration for override the gyu_id values
	 * @return object
	 */
	function toOne($name, $var = false) {
		
		$obj = new $name;
		if(!is_object($obj))
			return false;

		$config = array(
			'from' => $this->gyu_id,
			'to' => $obj->gyu_id
		);

		if($var)
			$config = array_merge($config, $var);

		if($config["to"] != $obj->gyu_id)
			$results = $obj->filter(array($config["to"], $this->{$config["from"]}), 'ONE');
		else
			$results = $obj->get($this->{$config["from"]});

		return $results;

	}

	/**
	 * Database Layer, Relation Manager
	 * $this to Many.
	 * ->RManyObjectName
	 * 
	 * @param  string $name Name of Object
	 * @param  mixed $var Configuration of override the gyu_id values
	 * @param  array $args List of conditions/option 
	 * @return array
	 */
	function toMany($name, $var = false, $args = false) {

		$obj = new $name;
		if(!is_object($obj))
			return false;

		$config = array(
			'from' => $this->gyu_id,
			'to' => $this->gyu_id
		);

		if($var)
			$config = array_merge($config, $var);

		$filterArgs[] = array($config["to"], $this->{$config["from"]});
		if(is_array($args))
			foreach($args as $arg) $filterArgs[] = $arg;

		$results = $obj->filterExecute($filterArgs);

		return $results;

	}

	# Magic!

	/**
	 * Database Layer
	 * Determine the table name (prefix manager)
	 * 
	 * @return false
	 */
	public function gyu_tablesPrefix() {

		if(isset($this->gyu_table)) {
			if($this->gyu_table[0] != '*')
				$this->gyu_table = tablesPrefix . $this->gyu_table;
			else
				$this->gyu_table = str_replace('*', '', $this->gyu_table);
		}

	}

	/**
	 * Caller
	 * 
	 * @param  string $name Name of Method
	 * @param  array $arguments List of Arguments
	 * @return mixed
	 */
	public function __call($name, $arguments) {

		$magics = ['ROne', 'RMany'];
		$magics_name = ['ROne' => 'toOne', 'RMany' => 'toMany'];

		foreach($magics as $magic)
			if(stristr($name, $magic)) {

				$name = strtolower(str_replace($magic, '', $name));
				$args[] = $name;
				foreach($arguments as $a)
					$args[] = $a;
				
				return call_user_func_array(array($this, $magics_name[$magic]), $args);

			}

		$error = '"' . $name . '" method does not exists';
		deb_error($error);
		
		return $error;

	}

}
