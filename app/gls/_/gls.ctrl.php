<?php


class glsCtrl extends standardController {

	var $index_tollerant = false;

	function __construct() {
		$this->ordini = LoadClass('ordini', 1);
		$this->gls = LoadClass('gls', 1); 
	}
	
	

	function CtrlCheckaddress() {  

		// echo 'dentro /gls/checkaddress';
		// echo '<br>con $_REQUEST["id_order"] = '.$_REQUEST["id_order"].'<br>';

		if(strlen($_REQUEST["SiglaProvincia"])>0){
	     	$siglaprovincia = LoadApp('prestashop', 1)->getProvincia($_REQUEST["SiglaProvincia"]);
	    }else{
	    	$siglaprovincia = "LT";
	    }

		

	    $soapUrl = "https://weblabeling.gls-italy.com/utility/wsCheckAddress.asmx?op=CheckAddress";
	    
	    
	    $sedegls = "LT";
	    
	    $codiceclientegls = "10747";
	    $passwordclientegls = "10747";
	    $id_order = $_REQUEST["id_order"];

	    



	    

	    if(strlen($_REQUEST["Cap"])>0){
	     	$cap = $_REQUEST["Cap"];
	    }else{
	    	$cap = "04100";
	    }

	    if(strlen($_REQUEST["Localita"])>0){
	     	$localita = $_REQUEST["Localita"];
	    }else{
	    	$localita = "Latina";
	    }

	    if(strlen($_REQUEST["Indirizzo"])>0){
	    	$indirizzo = $_REQUEST["Indirizzo"];
	    }else{
	    	$indirizzo = "Via Bramante 21";
	    }
	    
	    // xml post structure

	 

	    $xml_post_string = '<?xml version="1.0" encoding="UTF-8"?>
<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="http://weblabeling.gls-italy.com/">
  <SOAP-ENV:Body>
    <ns1:CheckAddress>
      <ns1:SedeGls>'.$sedegls.'</ns1:SedeGls>
      <ns1:CodiceClienteGls>'.$codiceclientegls.'</ns1:CodiceClienteGls>
      <ns1:PasswordClienteGls>'.$passwordclientegls.'</ns1:PasswordClienteGls>
      <ns1:SiglaProvincia>'.$siglaprovincia.'</ns1:SiglaProvincia>
      <ns1:Cap>'.$cap.'</ns1:Cap>
      <ns1:Localita>'.$localita.'</ns1:Localita>
      <ns1:Indirizzo>'.$indirizzo.'</ns1:Indirizzo>
    </ns1:CheckAddress>
  </SOAP-ENV:Body>
</SOAP-ENV:Envelope>';

	    //$xml_post_string = " ";

		$action = "http://weblabeling.gls-italy.com/CheckAddress";
	    $one_way = 0;
	    $version = "1.1";
		
		$client = new SoapClient(null, array('location' => "http://weblabeling.gls-italy.com",'uri' => "http://weblabeling.gls-italy.com"));
		$testreq = $client->__doRequest($xml_post_string, $soapUrl, $action, $version, $one_way);

		//var_dump($testreq);
	    // echo $xml_post_string;
	    //die(); 
	    $pieces = explode("CheckAddressResult", $testreq);

	    $myXMLData ="<?xml version='1.0' encoding='UTF-8'?><CheckAddressResult ".$pieces[1]."CheckAddressResult>";

	    // echo '<pre>'.$myXMLData.'</pre>';
	    // die();
		
		
		$objresp = new SimpleXMLElement($myXMLData);
		//$objresp = new SimpleXMLElement($testreq);
		
		if ($objresp === false) {
		    echo "Failed loading XML: ";
		    foreach(libxml_get_errors() as $error) {
		        echo "<br>", $error->message;
		    }
		    die();
		} else {
		    //echo 'Esito: '.$objresp->AddressList->Esito;
		    if(strlen(stristr($objresp->AddressList->Esito, "corretta"))>0){
		    	//echo 'ok destinazione';
		    	
		    	//print_r($objresp);
		    	$esito = "ok";
		    	
		    	Database()->query("INSERT INTO `validated_addresses`(`creationTime`, `id_order`, `siglaRegione`, `descRegione`, `siglaProvincia`, `descProvincia`, `cap`, `frazione`, `comune`, `indirizzo`, `inoltroInProvincia`, `kmDistanzaSede`, `sede`, `zona`, `tempoDiResa`, `localitaDisagiata`, `ztl`, `express12`, `identPIN`) VALUES (
		    		'".time()."','".$id_order."','".$objresp->AddressList->Address->SiglaRegione."','".addslashes($objresp->AddressList->Address->DescrizioneRegione)."','".$objresp->AddressList->Address->SiglaProvincia."','".addslashes($objresp->AddressList->Address->DescrizioneProvincia)."','".$objresp->AddressList->Address->Cap."','".addslashes($objresp->AddressList->Address->Frazione)."','".addslashes($objresp->AddressList->Address->Comune)."','".addslashes($objresp->AddressList->Address->Indirizzo)."','".$objresp->AddressList->Address->InoltroInProvincia."','".$objresp->AddressList->Address->KmDistanzaSede."','".$objresp->AddressList->Address->Sede."','".$objresp->AddressList->Address->Zona."','".$objresp->AddressList->Address->TempoDiResa."','".$objresp->AddressList->Address->LocalitaDisagiata."','".$objresp->AddressList->Address->ZTL."','".$objresp->AddressList->Address->Express12."','".$objresp->AddressList->Address->IdentPIN."')");
		    	

		    }else{
		    	$esito = "ko";
		    	// echo 'destinazione ko<br>';
		    	// echo '<pre>';
		    	//print_r($objresp);
		    	// echo '</pre>';
		    	//die();

		    	//$arraddresses = array();
		    	//$indice = 0;
		    	Database()->query("DELETE FROM `alternative_addresses` WHERE `id_order` = '".$id_order."'");
		    	
		    	foreach ($objresp->AddressList->Address as $singoloindirizzo) {
		    		Database()->query("INSERT INTO `alternative_addresses`(`creationTime`, `id_order`, `siglaRegione`, `descRegione`, `siglaProvincia`, `descProvincia`, `cap`, `frazione`, `comune`, `indirizzo`, `inoltroInProvincia`, `kmDistanzaSede`, `sede`, `zona`, `tempoDiResa`, `localitaDisagiata`, `ztl`, `express12`, `identPIN`) VALUES (
		    		'".time()."','".$id_order."','".$singoloindirizzo->SiglaRegione."','".addslashes($singoloindirizzo->DescrizioneRegione)."','".$singoloindirizzo->SiglaProvincia."','".addslashes($singoloindirizzo->DescrizioneProvincia)."','".$singoloindirizzo->Cap."','".addslashes($singoloindirizzo->Frazione)."','".addslashes($singoloindirizzo->Comune)."','".addslashes($singoloindirizzo->Indirizzo)."','".$singoloindirizzo->InoltroInProvincia."','".$singoloindirizzo->KmDistanzaSede."','".$singoloindirizzo->Sede."','".$singoloindirizzo->Zona."','".$singoloindirizzo->TempoDiResa."','".$singoloindirizzo->LocalitaDisagiata."','".$singoloindirizzo->ZTL."','".$singoloindirizzo->Express12."','".$singoloindirizzo->IdentPIN."')");
		    	

		    	}

		    	//$_SESSION["listaddr"] = $arraddresses;

		    	// print_r($_SESSION["listaddr"]);
		    	// die();



		    }
		}


		header('Location: /spedizioni/dettaglio/id_order:'.$id_order.'/esito:'.$esito.'');

	    	/**
	       $headers = array(
	     				"POST /utility/wsCheckAddress.asmx HTTP/1.1",
						"Host: weblabeling.gls-italy.com",
	                    "Content-type: text/xml;charset=\"utf-8\"",
	                    "Accept: text/xml",
	                    "SOAPAction: http://weblabeling.gls-italy.com/CheckAddress", 
	                    "Content-length: ".strlen($xml_post_string),
	                    //"Content-length: 100",
	                ); //SOAPAction: your op URL

	        $url = $soapUrl;

	        $ch = curl_init();
	        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	        curl_setopt($ch, CURLOPT_URL, $url);
	        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
	        curl_setopt($ch, CURLOPT_POST, true);
	        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string); // the SOAP request
	       	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

	        // converting
	        $response = curl_exec($ch); 
	        //var_dump(curl_errno($ch));
	        if(curl_errno($ch)){
	            echo 'Curl error: ' . curl_error($ch);
	        }               
	        curl_close($ch);

	        print_r($response);
	        die();

	        return $response;
	        **/
		
	}


	function CtrlAddparcel() {  

		//echo 'dentro /gls/checkaddress';
		// echo '<br>con $_REQUEST["id_order"] = '.$_REQUEST["id_order"].'<br>';

		$ordinedettaglio = LoadApp('prestashop', 1)->Invoke('orders', 'single');

		// echo '<pre>';
		// print_r($ordinedettaglio);
		// echo '</pre>';
		// die();

		if(strlen($_REQUEST["SiglaProvincia"])>0){
	     	$siglaprovincia = LoadApp('prestashop', 1)->getProvincia($_REQUEST["SiglaProvincia"]);
	    }else{
	    	$siglaprovincia = LoadApp('prestashop', 1)->getProvincia($ordinedettaglio->obj["shipping_address"]->id_state);
	    }

		

	    $soapUrl = "https://weblabeling.gls-italy.com/IlsWebService.asmx?op=AddParcel";
	    
	    
	    $sedegls = "LT";
	    
	    $codiceclientegls = "10747";
	    $passwordclientegls = "10747";
	    $codicecontrattogls = "459";


	    $id_order = $_REQUEST["id_order"];
	    



	    if(strlen($_REQUEST["Ragionesociale"])>0){
	     	$ragsoc = $_REQUEST["Ragionesociale"];
	    }else{
	    	$ragsoc = "";
	    	if(strlen($ordinedettaglio->obj["shipping_address"]->company)>0){
                        $ragsoc = $ragsoc.$ordinedettaglio->obj["shipping_address"]->company.' - ';    
            }
                            
            $ragsoc = $ragsoc.$ordinedettaglio->obj["shipping_address"]->firstname.' '.$ordinedettaglio->obj["shipping_address"]->lastname.'';
                  
	    	
	    }

	    
	    if(strlen($_REQUEST["Cap"])>0){
	     	$cap = $_REQUEST["Cap"];
	    }else{
	    	$cap = $ordinedettaglio->obj["shipping_address"]->postcode;
	    }

	    if(strlen($_REQUEST["Localita"])>0){
	     	$localita = $_REQUEST["Localita"];
	    }else{
	    	$localita = $ordinedettaglio->obj["shipping_address"]->city;
	    }

	    if(strlen($_REQUEST["Indirizzo"])>0){
	    	$indirizzo = $_REQUEST["Indirizzo"];
	    }else{
	    	$indirizzo = addslashes($ordinedettaglio->obj["shipping_address"]->address1.' '.$ordinedettaglio->obj["shipping_address"]->address2);
	    }


	    if(strlen($_REQUEST["PesoReale"])>0){
	    	$pesoreale = $_REQUEST["PesoReale"];
	    }else{
	    	$pesoreale = 0;
	    	foreach ($ordinedettaglio->obj["products"] as $prod) {
	    		$pesoreale = $pesoreale+$prod->product_weight;
	    	}
	    	if($pesoreale==0){
	    		$pesoreale = 0.5;
	    	}

	    }


	    if(strlen($_REQUEST["TipoPorto"])>0){
	    	$tipoporto = $_REQUEST["TipoPorto"];
	    }else{
	    	if((float)$ordinedettaglio->total_paid_tax_incl>150){
	    		$tipoporto = "F";
	    	}else{
	    		$tipoporto = "A";
	    	}
	    	
	    }

	    if(strlen($_REQUEST["TipoCollo"])>0){
	    	$tipocollo = $_REQUEST["TipoCollo"];
	    }else{
	    	$tipocollo = "0";
	    }


	    
	    // xml post structure



		$xml_post_string = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:web="http://weblabeling.gls-italy.com/">

   <soapenv:Header/>

   <soapenv:Body>

      <web:AddParcel>

         <web:XMLInfoParcel><![CDATA[ 

         	<Info>
			      <SedeGls>'.$sedegls.'</SedeGls>
			      <CodiceClienteGls>'.$codiceclientegls.'</CodiceClienteGls>
			      <PasswordClienteGls>'.$passwordclientegls.'</PasswordClienteGls>
			      <Parcel>
				      <CodiceContrattoGls>'.$codicecontrattogls.'</CodiceContrattoGls>
				      <RagioneSociale>'.$ragsoc.'</RagioneSociale>
				      <Indirizzo>'.$indirizzo.'</Indirizzo>
				      <Localita>'.$localita.'</Localita>
				      <Zipcode>'.$cap.'</Zipcode>
				      <Provincia>'.$siglaprovincia.'</Provincia>
				      <Colli>1</Colli>
				      <PesoReale>'.$pesoreale.'</PesoReale>
				      <TipoPorto>'.$tipoporto.'</TipoPorto>
				      <TipoCollo>'.$tipocollo.'</TipoCollo>
				  </Parcel>
			    </Info>]]>

		 </web:XMLInfoParcel>

      </web:AddParcel>

   </soapenv:Body>

</soapenv:Envelope>';
		

	    //$xml_post_string = " ";
	    $action = "http://weblabeling.gls-italy.com/AddParcel";
	    $one_way = 0;
	    $version = "1.1";

	    $client = new SoapClient(null, array('location' => "http://weblabeling.gls-italy.com",'uri' => "http://weblabeling.gls-italy.com"));
		$testreq = $client->__doRequest($xml_post_string, $soapUrl, $action, $version, $one_way);
		

		//$stringresp = "<pre>".htmlentities(str_ireplace('><', ">\n<", $testreq)) . "\n"."</pre>";
		
		$pieces = explode("AddParcelResponse", $testreq);
		// echo $pieces[1];
		// echo '<br><hr/><br>'; 


		
		$myXMLData ="<?xml version='1.0' encoding='UTF-8'?><AddParcelResponse ".$pieces[1]."AddParcelResponse>";
		
		// $xml = simplexml_load_string($myXMLData);

		// print_r($xml);

		
		// echo '$xml->DenominazioneDestinatario: '.$xml->DenominazioneDestinatario;
		// die();
		
		$objresp = new SimpleXMLElement($myXMLData);
		//$objresp = new SimpleXMLElement($testreq);
		
		if ($objresp === false) {
		    echo "Failed loading XML: ";
		    foreach(libxml_get_errors() as $error) {
		        echo "<br>", $error->message;
		    }
		    die();
		} else {
			// echo '<pre>';
		 //    print_r($objresp);
		 //    echo '</pre>';
		 //    die();

		 //    echo $objresp->AddParcelResult->InfoLabel->Parcel->DenominazioneMittente;
		 //    echo '<br>Me()->id: '.Me()->id;

		    $SiglaMittente = $objresp->AddParcelResult->InfoLabel->Parcel->SiglaMittente;
		    $NumeroSpedizione = $objresp->AddParcelResult->InfoLabel->Parcel->NumeroSpedizione;
		    $TotaleColli = $objresp->AddParcelResult->InfoLabel->Parcel->TotaleColli;
		    $TipoCollo = $objresp->AddParcelResult->InfoLabel->Parcel->TipoCollo;
		    $SiglaSedeDestino = $objresp->AddParcelResult->InfoLabel->Parcel->SiglaSedeDestino;
		    $DenominazioneMittente = addslashes($objresp->AddParcelResult->InfoLabel->Parcel->DenominazioneMittente);
		    $DenominazioneDestinatario = addslashes($objresp->AddParcelResult->InfoLabel->Parcel->DenominazioneDestinatario);
		    $IndirizzoDestinatario = addslashes($objresp->AddParcelResult->InfoLabel->Parcel->IndirizzoDestinatario);
		    $CittaDestinatario = addslashes($objresp->AddParcelResult->InfoLabel->Parcel->CittaDestinatario);
		    $ProvinciaDestinatario = addslashes($objresp->AddParcelResult->InfoLabel->Parcel->ProvinciaDestinatario);
		    $DataSpedizione = $objresp->AddParcelResult->InfoLabel->Parcel->DataSpedizione;
		    $DescrizioneSedeDestino = addslashes($objresp->AddParcelResult->InfoLabel->Parcel->DescrizioneSedeDestino);
		    $PesoSpedizione = $objresp->AddParcelResult->InfoLabel->Parcel->PesoSpedizione;
		    $ImportoAssegnato = $objresp->AddParcelResult->InfoLabel->Parcel->ImportoAssegnato;
		    $ImportoCassegno = $objresp->AddParcelResult->InfoLabel->Parcel->ImportoCassegno;
		    $TotaleImportodaIncassare = $objresp->AddParcelResult->InfoLabel->Parcel->TotaleImportodaIncassare;
		    $TelefonoSede = $objresp->AddParcelResult->InfoLabel->Parcel->TelefonoSede;
		    $NoteSpedizione = addslashes($objresp->AddParcelResult->InfoLabel->Parcel->NoteSpedizione);
		    $DescrizioneTipoPorto = addslashes($objresp->AddParcelResult->InfoLabel->Parcel->DescrizioneTipoPorto);
		    $SiglaCSM = $objresp->AddParcelResult->InfoLabel->Parcel->SiglaCSM;
		    $DescrizioneCSM1 = addslashes($objresp->AddParcelResult->InfoLabel->Parcel->DescrizioneCSM1);
		    $DescrizioneCSM2 = addslashes($objresp->AddParcelResult->InfoLabel->Parcel->DescrizioneCSM2);
		    $Percorso1 = addslashes($objresp->AddParcelResult->InfoLabel->Parcel->Percorso1);
		    $Percorso2 = addslashes($objresp->AddParcelResult->InfoLabel->Parcel->Percorso2);
		    $Percorso3 = addslashes($objresp->AddParcelResult->InfoLabel->Parcel->Percorso3);
		    $RapportoPesoVolume = $objresp->AddParcelResult->InfoLabel->Parcel->RapportoPesoVolume;
		    $ProgressivoCollo = $objresp->AddParcelResult->InfoLabel->Parcel->ProgressivoCollo;
		    $CodiceZona = addslashes($objresp->AddParcelResult->InfoLabel->Parcel->CodiceZona);
		    $RiferimentiCliente = addslashes($objresp->AddParcelResult->InfoLabel->Parcel->RiferimentiCliente);
		    $Reverse = $objresp->AddParcelResult->InfoLabel->Parcel->Reverse;
		    $Sprinter = $objresp->AddParcelResult->InfoLabel->Parcel->Sprinter;
		    $Bda = $objresp->AddParcelResult->InfoLabel->Parcel->Bda;
		    $ContatoreProgressivo = $objresp->AddParcelResult->InfoLabel->Parcel->ContatoreProgressivo;
		    $PdfLabel = $objresp->AddParcelResult->InfoLabel->Parcel->PdfLabel;
		    $Zpl = $objresp->AddParcelResult->InfoLabel->Parcel->Zpl;

		    //echo '<br>Query per AddParcel:<br>';
		    $stringquery = "INSERT INTO `added_parcel`(`creationTime`, `id_order`, 
		    	`id_user`, `SiglaMittente`, `NumeroSpedizione`, `TotaleColli`, `TipoCollo`, 
		    	`SiglaSedeDestino`, `DenominazioneMittente`, `DenominazioneDestinatario`, `IndirizzoDestinatario`, 
		    	`CittaDestinatario`, `ProvinciaDestinatario`, `DataSpedizione`, `DescrizioneSedeDestino`, 
		    	`PesoSpedizione`, `ImportoAssegnato`, `ImportoCassegno`, `TotaleImportodaIncassare`, `TelefonoSede`, 
		    	`NoteSpedizione`, `DescrizioneTipoPorto`, `SiglaCSM`, `DescrizioneCSM1`, `DescrizioneCSM2`, 
		    	`Percorso1`, `Percorso2`, `Percorso3`, `RapportoPesoVolume`, `ProgressivoCollo`, `CodiceZona`, 
		    	`RiferimentiCliente`, `Reverse`, `Sprinter`, `Bda`, `ContatoreProgressivo`, `PdfLabel`, `Zpl`) 
		    VALUES ('".time()."','".$id_order."','".Me()->id."','".$SiglaMittente."','".$NumeroSpedizione."','".$TotaleColli."'
		    	,'".$TipoCollo."','".$SiglaSedeDestino."','".$DenominazioneMittente."','".$DenominazioneDestinatario."'
		    	,'".$IndirizzoDestinatario."','".$CittaDestinatario."','".$ProvinciaDestinatario."','".$DataSpedizione."'
		    	,'".$DescrizioneSedeDestino."','".$PesoSpedizione."','".$ImportoAssegnato."','".$ImportoCassegno."'
		    	,'".$TotaleImportodaIncassare."','".$TelefonoSede."','".$NoteSpedizione."','".$DescrizioneTipoPorto."'
		    	,'".$SiglaCSM."','".$DescrizioneCSM1."','".$DescrizioneCSM2."','".$Percorso1."','".$Percorso2."'
		    	,'".$Percorso3."','".$RapportoPesoVolume."','".$ProgressivoCollo."','".$CodiceZona."','".$RiferimentiCliente."'
		    	,'".$Reverse."','".$Sprinter."','".$Bda."','".$ContatoreProgressivo."','".$PdfLabel."','".$Zpl."')";

			// echo $stringquery;
			// die();

		   
		    Database()->query($stringquery); 
			


		}
		

	 	//die(); 

	    header('Location: /spedizioni/index');
   
		
	}


	function CtrlCloseworkday() {  

		//echo 'dentro /gls/checkaddress';
		// echo '<br>con $_REQUEST["id_order"] = '.$_REQUEST["id_order"].'<br>';

		$ordinedettaglio = LoadApp('prestashop', 1)->Invoke('orders', 'single');

		// echo '<pre>';
		// print_r($ordinedettaglio);
		// echo '</pre>';
		// die();

		if(strlen($_REQUEST["SiglaProvincia"])>0){
	     	$siglaprovincia = LoadApp('prestashop', 1)->getProvincia($_REQUEST["SiglaProvincia"]);
	    }else{
	    	$siglaprovincia = LoadApp('prestashop', 1)->getProvincia($ordinedettaglio->obj["shipping_address"]->id_state);
	    }

		

	    $soapUrl = "https://weblabeling.gls-italy.com/IlsWebService.asmx?op=CloseWorkDay";
	    
	    
	    $sedegls = "LT";
	    
	    $codiceclientegls = "10747";
	    $passwordclientegls = "10747";
	    $codicecontrattogls = "459";


	    $id_order = $_REQUEST["id_order"];
	    



	    if(strlen($_REQUEST["Ragionesociale"])>0){
	     	$ragsoc = $_REQUEST["Ragionesociale"];
	    }else{
	    	$ragsoc = "";
	    	if(strlen($ordinedettaglio->obj["shipping_address"]->company)>0){
                        $ragsoc = $ragsoc.$ordinedettaglio->obj["shipping_address"]->company.' - ';    
            }
                            
            $ragsoc = $ragsoc.$ordinedettaglio->obj["shipping_address"]->firstname.' '.$ordinedettaglio->obj["shipping_address"]->lastname.'';
                  
	    	
	    }

	    
	    if(strlen($_REQUEST["Cap"])>0){
	     	$cap = $_REQUEST["Cap"];
	    }else{
	    	$cap = $ordinedettaglio->obj["shipping_address"]->postcode;
	    }

	    if(strlen($_REQUEST["Localita"])>0){
	     	$localita = $_REQUEST["Localita"];
	    }else{
	    	$localita = $ordinedettaglio->obj["shipping_address"]->city;
	    }

	    if(strlen($_REQUEST["Indirizzo"])>0){
	    	$indirizzo = $_REQUEST["Indirizzo"];
	    }else{
	    	$indirizzo = addslashes($ordinedettaglio->obj["shipping_address"]->address1.' '.$ordinedettaglio->obj["shipping_address"]->address2);
	    }


	    if(strlen($_REQUEST["PesoReale"])>0){
	    	$pesoreale = $_REQUEST["PesoReale"];
	    }else{
	    	$pesoreale = 0;
	    	foreach ($ordinedettaglio->obj["products"] as $prod) {
	    		$pesoreale = $pesoreale+$prod->product_weight;
	    	}
	    	if($pesoreale==0){
	    		$pesoreale = 0.5;
	    	}

	    }


	    if(strlen($_REQUEST["TipoPorto"])>0){
	    	$tipoporto = $_REQUEST["TipoPorto"];
	    }else{
	    	if((float)$ordinedettaglio->total_paid_tax_incl>150){
	    		$tipoporto = "F";
	    	}else{
	    		$tipoporto = "A";
	    	}
	    	
	    }

	    if(strlen($_REQUEST["TipoCollo"])>0){
	    	$tipocollo = $_REQUEST["TipoCollo"];
	    }else{
	    	$tipocollo = "0";
	    }


	    
	    // xml post structure



		$xml_post_string = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:web="http://weblabeling.gls-italy.com/">

   <soapenv:Header/>

   <soapenv:Body>

      <web:CloseWorkDay>

         <!--Optional:-->

         <web:XMLCloseInfoParcel><![CDATA[  

         	<Info>
			      <SedeGls>'.$sedegls.'</SedeGls>
			      <CodiceClienteGls>'.$codiceclientegls.'</CodiceClienteGls>
			      <PasswordClienteGls>'.$passwordclientegls.'</PasswordClienteGls>
			      <Parcel>
				      <CodiceContrattoGls>'.$codicecontrattogls.'</CodiceContrattoGls>
				      <RagioneSociale>'.$ragsoc.'</RagioneSociale>
				      <Indirizzo>'.$indirizzo.'</Indirizzo>
				      <Localita>'.$localita.'</Localita>
				      <Zipcode>'.$cap.'</Zipcode>
				      <Provincia>'.$siglaprovincia.'</Provincia>
				      <Colli>1</Colli>
				      <PesoReale>'.$pesoreale.'</PesoReale>
				      <TipoPorto>'.$tipoporto.'</TipoPorto>
				      <TipoCollo>'.$tipocollo.'</TipoCollo>
				      <NumDayListSped>1</NumDayListSped>
				  </Parcel>
			    </Info>]]>

		 </web:XMLCloseInfoParcel>

      </web:CloseWorkDay>

   </soapenv:Body>

</soapenv:Envelope>';



		// echo '<pre>';

		// echo $xml_post_string;

		// echo '</pre>';
		// die();
		

	    //$xml_post_string = " ";
	    $action = "http://weblabeling.gls-italy.com/CloseWorkDay";
	    $one_way = 0;
	    $version = "1.1"; 

	    $client = new SoapClient(null, array('location' => "http://weblabeling.gls-italy.com",'uri' => "http://weblabeling.gls-italy.com"));
		$testreq = $client->__doRequest($xml_post_string, $soapUrl, $action, $version, $one_way);
		
		

		//$stringresp = "<pre>".htmlentities(str_ireplace('><', ">\n<", $testreq)) . "\n"."</pre>";
		
		$pieces = explode("ListParcel", $testreq);
		// echo $pieces[1];
		// echo '<br><hr/><br>'; 


		
		$myXMLData ='<?xml version="1.0" encoding="utf-8"?><soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><soap:Body><CloseWorkDayResponse xmlns="http://weblabeling.gls-italy.com/"><CloseWorkDayResult><ListParcel '.$pieces[1].'ListParcel></CloseWorkDayResult></CloseWorkDayResponse></soap:Body></soap:Envelope>';
		
		// $xml = simplexml_load_string($myXMLData);

		// print_r($xml);

		
		// echo '$xml->DenominazioneDestinatario: '.$xml->DenominazioneDestinatario;
		// die();
		
		$objresp = new SimpleXMLElement($myXMLData);
		//$objresp = new SimpleXMLElement($testreq);
		
		if ($objresp === false) {
		    echo "Failed loading XML: ";
		    foreach(libxml_get_errors() as $error) {
		        echo "<br>", $error->message;
		    }
		    die();
		} else {
			// echo '<pre>';
		 //    print_r($objresp);
		 //    echo '</pre>';
		 //    die();

		 //    echo $objresp->AddParcelResult->InfoLabel->Parcel->DenominazioneMittente;
		 //    echo '<br>Me()->id: '.Me()->id;

		    $stringquery = "INSERT INTO `closed_workday`(`creationTime`, `id_order`, `id_user`) VALUES ('".time()."','".$id_order."','".Me()->id."')";

		    

		   
		    Database()->query($stringquery); 

		    LoadApp('prestashop', 1)->setstatord($id_order,4);
			


		}
		

	 	//die(); 

	    header('Location: /spedizioni/index');
   
		
	}

	function CtrlSelectaddress() { 

		// echo 'dentro selectaddress';
		// echo '<br>con $_REQUEST["Indirizzo"]: '.$_REQUEST["Indirizzo"];
		// die();
		$id_order = $_REQUEST["id_order"];
		$SiglaRegione = $_REQUEST["SiglaRegione"];
		$DescrizioneRegione = $_REQUEST["DescrizioneRegione"];
		$SiglaProvincia = $_REQUEST["SiglaProvincia"];
		$DescrizioneProvincia = $_REQUEST["DescrizioneProvincia"];
		$Cap = $_REQUEST["Cap"];
		$Frazione = $_REQUEST["Frazione"];
		$Comune = $_REQUEST["Comune"];
		$Indirizzo = $_REQUEST["Indirizzo"];
		$InoltroInProvincia = $_REQUEST["InoltroInProvincia"];
		$KmDistanzaSede = $_REQUEST["KmDistanzaSede"];
		$Sede = $_REQUEST["Sede"];
		$Zona = $_REQUEST["Zona"];
		$TempoDiResa = $_REQUEST["TempoDiResa"];
		$LocalitaDisagiata = $_REQUEST["LocalitaDisagiata"];
		$ZTL = $_REQUEST["ZTL"];
		$Express12 = $_REQUEST["Express12"];
		$IdentPIN = $_REQUEST["IdentPIN"];


		Database()->query("INSERT INTO `validated_addresses`(`creationTime`, `id_order`, `siglaRegione`, `descRegione`, `siglaProvincia`, `descProvincia`, `cap`, `frazione`, `comune`, `indirizzo`, `inoltroInProvincia`, `kmDistanzaSede`, `sede`, `zona`, `tempoDiResa`, `localitaDisagiata`, `ztl`, `express12`, `identPIN`) VALUES (
		    		'".time()."','".$id_order."','".$SiglaRegione."','".($DescrizioneRegione)."','".$SiglaProvincia."','".($DescrizioneProvincia)."','".$Cap."','".($Frazione)."','".($Comune)."','".($Indirizzo)."','".$InoltroInProvincia."','".$KmDistanzaSede."','".$Sede."','".$Zona."','".$TempoDiResa."','".$LocalitaDisagiata."','".$ZTL."','".$Express12."','".$IdentPIN."')");

		$esito = 'ok';

		header('Location: /spedizioni/dettaglio/id_order:'.$id_order.'/esito:'.$esito.'');

		    	

	}

}

?>