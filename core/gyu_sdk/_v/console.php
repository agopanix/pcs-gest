<?

$inkb = function($value) { return round($value / (1024*100), 2) . ' KB'; };
$inmb = function($value) { return round($value / (1024*1000), 2) . ' MB'; };

?>
<script type="text/javascript">

	function dev_exec() {
		$(document).on('click', '[data-show]', function() {
			$($(this).data('show')).toggle();
		});
	};

	if(typeof jQuery == 'undefined') {
		var script = document.createElement('script');
		script.type = "text/javascript";
		script.src = "http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js";
		script.onload = function () {
	        dev_exec();
	    };
		document.getElementsByTagName('head')[0].appendChild(script);
	} else {
		dev_exec();
	}

</script>
<fieldset id="gyu_debugger_console" class="gyu_debugger_console" style="width: 80%; margin: 1em auto">
	<? if(isset($app_data["env"]->error)): ?>
		<? if(isset($app_data["env"]->errorStop)): ?>
			<h2>AN ERROR HAS STOPPED THE EXECUTION!!!11111!!1</h2>
		<? else: ?>
			<h2>There's some errors in the execution stack.</h2>
		<? endif; ?>
		<strong>Errors</strong>
		<ul>
			<? foreach($app_data["env"]->errorMessages as $k => $message): ?>
			<li>
				<? echo $message["message"]; ?><br />
				<a href="javascript:;" data-show=".dev_error_<? echo $k; ?>">Details</a>
				<div class="dev_error_<? echo $k; ?>" style="display: none">
					<? foreach($message["debug"]["info2"] as $l => $i): ?>
						<em><? echo $l+1; ?> <u>of</u> <? echo count($message["debug"]["info2"]); ?></em><br />
						<pre style="font-size: 11px"><strong>File:</strong> <? echo($i["file"]); ?></pre>
						<pre style="font-size: 11px"><strong>Line:</strong> <? echo($i["line"]); ?></pre>
						<pre style="font-size: 11px"><strong>Function:</strong> <? echo($i["function"]); ?></pre>
						<? if(isset($i["class"])): ?>
							<pre style="font-size: 11px"><strong>Class:</strong> <? print_r($i["object"]); ?></pre>
						<? endif; ?>
						<hr />
					<? endforeach; ?>
				</div>
			</li>
			<? endforeach; ?>
		</ul>
		<hr />
	<? endif; ?>
	<legend>Console</legend>
	<a href="#performance">Performance Area</a>, 
	<a href="#environment" data-show=".dev_env">Environment</a>, 
	<a href="#routes">Routes</a>, 
	<a href="#methods">Methods</a>, 
	<a href="#stack" data-show=".dev_stack">Stack</a>

	<fieldset>
		<legend id="performance">Performance Area</legend>
		
		<strong>Memory Usage</strong><br /><br />
		<table width="100%">
			<thead>
				<tr>
					<th>Initial Memory</th>
					<th>Final Memory</th>
					<th>Peak Memory</th>
					<th>Delta</th>
					<th>Projection (in KB)</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td><? echo $app_data["memory"]["__startMemoryUsage"]; ?> (<? echo $inkb($app_data["memory"]["__startMemoryUsage"]); ?>) (<? echo $inmb($app_data["memory"]["__startMemoryUsage"]); ?>)</td>
					<td><? echo $app_data["memory"]["__finalMemoryUsage"]; ?> (<? echo $inkb($app_data["memory"]["__finalMemoryUsage"]); ?>) (<? echo $inmb($app_data["memory"]["__finalMemoryUsage"]); ?>)</td>
					<td><? echo $app_data["memory"]["__peakMemoryUsage"]; ?> (<? echo $inkb($app_data["memory"]["__peakMemoryUsage"]); ?>) (<? echo $inmb($app_data["memory"]["__peakMemoryUsage"]); ?>)</td>
					<td><? echo $app_data["memory"]["delta"]; ?> (<? echo $inkb($app_data["memory"]["delta"]); ?>) (<? echo $inmb($app_data["memory"]["delta"]); ?>)</td>
					<td>
						<em>10 Users:</em> <? echo $app_data["memory"]["ten_user_resource_KB"]; ?><br />
						<em>100 Users:</em> <? echo $app_data["memory"]["hundred_user_resource_KB"]; ?><br />
						<em>1000 Users:</em> <? echo $app_data["memory"]["thousand_user_resource_MB"]; ?> MB<br />
					</td>
				</tr>
			</tbody>
		</table>

		<strong>Time Execution</strong><br /><br />
		<table width="100%">
			<thead>
				<tr>
					<th>Start Time</th>
					<th>End Time</th>
					<th>Duration</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td><? echo $app_data["execution"]["__start"]; ?></td>
					<td><? echo $app_data["execution"]["__end"]; ?></td>
					<td><? echo round($app_data["execution"]["__duration"], 4); ?> seconds</td>
				</tr>
			</tbody>
		</table>
		<strong>Database Queries</strong><br /><br />
		<? echo $app_data["nQuery"]; ?><br />
		<a href="javascript:;" data-show=".dev_queries">Show Queries</a>
		<div class="dev_queries" style="display: none">
			<? foreach($GLOBALS["queries_archive"] as $query): ?>
				<code style="font-size: 11px"><? echo $query["query"]; ?></code><br /><br />
			<? endforeach; ?>
		</div>

	</fieldset>

	<fieldset>
		<legend id="environment"><a data-show=".dev_env" href="javascript:;">Environment</a></legend>
		<pre class="dev_env" style="display: none"><? print_r($app_data["env"]); ?></pre>
	</fieldset>

	<fieldset>
		<legend id="routes">Routes/Applications</legend>
		<ul>
			<? foreach($app_data["routes"] as $k => $route): ?>
			<li style="margin-bottom: 0.5em">
				<code>#<? echo $k; ?> <? echo strlen($route->availableControllers) > 0 ? $route->availableControllers . ' <u>as</u> ' : ''; ?><? echo $route->candidate; ?></code> <a href="javascript:;" data-show=".dev_route_<? echo $k; ?>">show info</a>
				<pre class="dev_route_<? echo $k; ?>" style="display: none"><? print_r($route); ?></pre>
			</li>
			<? endforeach; ?>
		</ul>
	</fieldset>

		<fieldset>
		<legend id="methods">Methods</legend>
		<pre><? print_r($app_data["methods"]); ?></pre>
	</fieldset>

	<fieldset>
		<legend id="stack"><a data-show=".dev_stack" href="javascript:;">Stack</a></legend>
		<pre class="dev_stack" style="display: none"><? print_r($app_data["stack"]); ?></pre>
	</fieldset>
</fieldset>