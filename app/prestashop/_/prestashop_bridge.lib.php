<?php

class prestashop_bridge {

	protected $config;

	function __construct() {

		$this->config = json_decode(file_get_contents(__DIR__ . '/../_assets/config.json'));

		if(!is_object($this->config))
			throw new Exception("Error Processing Request", 1);
			

		$this->_layer = $this->config->type . ':host='.$this->config->host.';dbname='.$this->config->dbName.';charset=UTF8';
		$this->layer = new PDO($this->_layer, $this->config->username, $this->config->password);

	}

}