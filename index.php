<?php

/**
 * Gyural
 *
 * © 2016 - Mandarino Adv s.r.l.
 * Read /readme.md and /license.txt for further information.
 * http://www.mandarinoadv.com
 * http://gyural.com
 * 
 * @version 1.10
 * @author Federico Quagliotto <f.quagliotto@mandarinoadv.com>
 */
#die('sto');
define('gyu', true);

error_reporting(E_ALL); 
ini_set('display_errors','1');

// For a deeper debug, comment the following line.
error_reporting(0);

include_once('config.php');
include_once(__DIR__ . '/vendor/autoload.php');
include_once(absolute . 'funcs/gyural.php');

try {

	gyu_autoload();
	gyu_psr4();

	// hook:gyu.created
	\Gyu\Hooks::get('gyu.created', time());

	// Init Libs
	\Gyu\Hooks::get('gyu.init-libs');
	LoadClass('standardObject');
	LoadClass('standardController');

	// hook:gyu.after-libs
	\Gyu\Hooks::get('gyu.after-libs');

	// Database Connection
	defined('dbLink') && db == 1 ? $databaseDriver = ParseDatabase(dbLink, $dbLink_pattern) : null;

	session_start();

	if(is_file(absolute) . 'router.php')
		include absolute . 'router.php';

	$env = Route($page, null, $router);

	// hook:gyu.engine.start
	\Gyu\Hooks::get('gyu.engine.start', time());
	
	// What version? Showcase or fullyarmed?
	if(db == 1) is_object(Database()) ? \Gyu\Hooks::get('gyu.db.ready') : deb_error('Impossible connect to the database.', 1);
	
	// Runtime
	if(!defined('cli'))
		$env->exec();
	else
		if(function_exists('cli_handler')) cli_handler();

	// If you have to debug the environment, uncomment: 
	// $env->debug();

	if(isset($GLOBALS["gyu_buffer"]) && is_array($GLOBALS["gyu_buffer"])) ob_end_flush();
	
	// hook:gyu.engine.off
	\Gyu\Hooks::get('gyu.engine.off', time());
	
	if(is_object(Database())) Database()->close();
	
	// hook:gyu.off
	\Gyu\Hooks::get('gyu.off', time());
	
	// Check what hook to execute, based on the *dev* constant (hook:dev-zone|hook:dev-zone-hidden)
	if(dev)
		\Gyu\Hooks::get('gyu.dev-zone', time(), $env);
	else
		\Gyu\Hooks::get('gyu.dev-zone-hidden', time(), $env);
	
} catch (Exception $e) {
	deb_error($e, 1);
}
