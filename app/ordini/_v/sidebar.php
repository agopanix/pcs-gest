<div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
            <li class="filtro1 active"><a href="#" id="tutti">Tutti</a></li>
            <li class="filtro2 "><a href="#" id="accettati">Pagamento accettato</a></li>
            <li class="filtro3 "><a href="#" id="inlavorazione">Ordine in Lavorazione</a></li>
            <li class="filtro4 "><a href="#" id="inpreparazione">Preparazione in Corso</a></li>
            
          </ul>
          
</div>

<script>
$(document).ready(function(){
    $("#accettati").click(function(){
        $(".accettato").show();
        $(".lavorazione").hide();
        $(".preparazione").hide();
        $(".altro").hide();

        $(".filtro1").removeClass("active");
        $(".filtro2").addClass("active");
        $(".filtro3").removeClass("active");
        $(".filtro4").removeClass("active");
    });
    $("#inlavorazione").click(function(){
        $(".accettato").hide();
        $(".lavorazione").show();
        $(".preparazione").hide();
        $(".altro").hide();

        $(".filtro1").removeClass("active");
        $(".filtro2").removeClass("active");
        $(".filtro3").addClass("active");
        $(".filtro4").removeClass("active");
    });
    $("#inpreparazione").click(function(){
        $(".accettato").hide();
        $(".lavorazione").hide();
        $(".prreparazione").show();
        $(".altro").hide();

        $(".filtro1").removeClass("active");
        $(".filtro2").removeClass("active");
        $(".filtro3").removeClass("active");
        $(".filtro4").addClass("active");
    });
    $("#tutti").click(function(){
        $(".accettato").show();
        $(".lavorazione").show();
        $(".preparazione").show();
        $(".altro").show();

        $(".filtro1").addClass("active");
        $(".filtro2").removeClass("active");
        $(".filtro3").removeClass("active");
        $(".filtro4").removeClass("active");
    });
});
</script>