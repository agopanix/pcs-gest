<?php

/**
 * GYU - Cache
 *
 * Cache Manager
 *
 * @version 0.1
 * @author Federico Quagliotto <f.quagliotto@mandarinoadv.com>
 */

namespace Gyu;

class Cache {
	
	/**
	 * Return the path of the GyuCache
	 * 
	 * @return string
	 */
	function path() {
		$p = cache . 'GyuCache/';
		if(!is_dir($p))
			mkdir($p);
		return $p;
	}
	
	/**
	 * Retrive a cached data.
	 * 
	 * @param  string $key Name of the Cache Key
	 * @return mixed
	 */
	function get($key) {
		
		$fileBe = self::path() . '/' . md5($key);
		$content = file_get_contents($fileBe);
		
		if($content) {
			$parsed = unserialize($content);
			if($parsed["expire"] > time()) {
				return $parsed["cache"];
			} else {
				unlink($fileBe);
			}
		}
		
		return false;
		
	}
	
	/**
	 * Set a cache data.
	 * 
	 * @param string  $key      Name of the Cache Key
	 * @param mixed   $content
	 * @param integer $duration
	 */
	function set($key, $content, $duration = 3600) {
		
		$fileBe = self::path() . '/' . md5($key);
		
		if($duration == 'day') {
			$duration = strtotime('tomorrow midnight');
		} else {
			$duration += time();
		}
		
		$obj = array(
			'expire' => $duration,
			'cache' => $content
		);
		
		file_put_contents($fileBe, serialize($obj));

		return $obj["cache"];
		
	}
	
	/**
	 * Remove a cached data
	 * 
	 * @param  string $key Name of the Cache Key
	 */
	function erase($key) {
		
		$fileBe = self::path() . '/' . md5($key);
		if(is_file($fileBe))
			unlink($fileBe);
		
		return true;
		
	}
	
}