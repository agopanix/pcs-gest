<?php


class ordiniCtrl extends standardController {

	var $index_tollerant = false;

	function __construct() {
		$this->ordini = LoadClass('ordini', 1);
		$this->index = LoadClass('index', 1); 
	}
	
	function CtrlIndex() {  
		$this->last10ordini = LoadApp('prestashop', 1)->Invoke('orders', 'all');
		$this->index->headlogged();
		Application('ordini/_v/index', null, $this);
		$this->index->foot();
	}

	function CtrlDettaglio() {  
		$this->ordinedettaglio = LoadApp('prestashop', 1)->Invoke('orders', 'single');
		$this->allstates = LoadApp('prestashop', 1)->getCurrentstates();
		$this->index->headlogged();
		Application('ordini/_v/detail', null, $this);
		$this->index->foot();
	}

}

?>