<?php 
if(Logged()){
?>



    

    <div class="container-fluid">
      <div class="row">
        
        <?php 
            //include 'sidebar.php';
        ?>
        
        <div class="col-sm-12 col-md-12 main">
          <h1 class="page-header">Dettaglio Ordine</h1>

          
          <div class="jumbotron">
            <?php 
            // echo '<pre>';
            // print_r($app_data->ordinedettaglio);
            // echo '</pre>';

            $ordine = $app_data->ordinedettaglio;
            
              echo '<h2>Ordine #: '.$ordine->id_order.' ('.$ordine->reference.')</h2>';
               
            
            ?>
            
            
          </div>

          

          <div class="jumbotron">
            <?php 
            if(strlen($_GET["esito"])==0){ 

              $validato = FetchObject(Database()->query("SELECT * FROM `validated_addresses` WHERE id_order = '".$ordine->id_order."' ORDER BY id DESC LIMIT 1"),0);
    
              if(strlen($validato->id_order)>0){

                $inserito = FetchObject(Database()->query("SELECT * FROM `added_parcel` WHERE id_order = '".$ordine->id_order."' ORDER BY id DESC LIMIT 1"),0);
                
                if(strlen($inserito->id_order)>0){

                  $trasmesso = FetchObject(Database()->query("SELECT * FROM `closed_workday` WHERE id_order = '".$ordine->id_order."' ORDER BY id DESC LIMIT 1"),0);
                
                    if(strlen($trasmesso->id_order)>0){


                      echo '<p>';
                      echo '<form method="POST" action="/spedizioni/stampa">';
                        echo '<h3>Spedizione trasmessa a GLS in data: '.date('d-m-Y H:i',$trasmesso->creationTime).'</h3>';
                          echo 'Alias: '.$ordine->obj["shipping_address"]->alias.'<br>';
                          
                                    
                          echo 'Nome e Cognome: '.$inserito->DenominazioneDestinatario.'<br>';
                          echo 'Indirizzo: '.$inserito->IndirizzoDestinatario.'<br>';
                          echo 'CAP: '.$validato->cap.'<br>';
                          echo 'Città: '.$validato->comune.'<br>';
                          if(strlen($validato->frazione)>0){
                            echo 'Frazione: '.$validato->frazione.'<br>';
                          }
                          echo 'Provincia: '.$inserito->ProvinciaDestinatario.'<br>';
                          if(strlen($ordine->obj["shipping_address"]->phone)>0){
                            echo 'Telefono: '.$ordine->obj["shipping_address"]->phone.'<br>';
                          }
                          if(strlen($ordine->obj["shipping_address"]->phone_mobile)>0){
                            echo 'Cellulare: '.$ordine->obj["shipping_address"]->phone_mobile.'<br>';
                          }
                          if(strlen($ordine->obj["shipping_address"]->vat_number)>0){
                            echo 'P.Iva: '.$ordine->obj["shipping_address"]->vat_number.'<br>';
                          }
                          if(strlen($ordine->obj["shipping_address"]->dni)>0){
                            echo 'Cod.Fiscale: '.$ordine->obj["shipping_address"]->dni.'<br>';
                          }
                          if(strlen($ordine->obj["shipping_address"]->custom_tipologiaCliente)>0){
                            echo 'Tip. Cliente: '.$ordine->obj["shipping_address"]->custom_tipologiaCliente.'<br>';
                          } 

                          echo '<input type="hidden" name="id_order" value="'.$ordine->id_order.'"/>';

                          

                          
                          echo '<button type="submit" class="btn btn-danger">Stampa etichetta</button>';


                          echo '</form>';
                          
                          echo '</p>';

                    }else{


                  echo '<p>';
                echo '<form method="POST" action="/gls/closeworkday">';
                  echo '<h3>Indirizzo di Spedizione validato in data: '.date('d-m-Y H:i',$validato->creationTime).'</h3>';
                  echo 'Alias: '.$ordine->obj["shipping_address"]->alias.'<br>';
                  if(strlen($ordine->obj["shipping_address"]->company)>0){
                        echo 'Nome Azienda: '.$ordine->obj["shipping_address"]->company.'<br>';    
                  }
                            
                  echo 'Nome e Cognome: '.$ordine->obj["shipping_address"]->firstname.' '.$ordine->obj["shipping_address"]->lastname.'<br>';
                  echo 'Indirizzo: '.$validato->indirizzo.'<br>';
                  echo 'CAP: '.$validato->cap.'<br>';
                  echo 'Città: '.$validato->comune.'<br>';
                  if(strlen($validato->frazione)>0){
                    echo 'Frazione: '.$validato->frazione.'<br>';
                  }
                  echo 'Provincia: '.$validato->descProvincia.' ('.$validato->siglaProvincia.')<br>';
                  if(strlen($ordine->obj["shipping_address"]->phone)>0){
                    echo 'Telefono: '.$ordine->obj["shipping_address"]->phone.'<br>';
                  }
                  if(strlen($ordine->obj["shipping_address"]->phone_mobile)>0){
                    echo 'Cellulare: '.$ordine->obj["shipping_address"]->phone_mobile.'<br>';
                  }
                  if(strlen($ordine->obj["shipping_address"]->vat_number)>0){
                    echo 'P.Iva: '.$ordine->obj["shipping_address"]->vat_number.'<br>';
                  }
                  if(strlen($ordine->obj["shipping_address"]->dni)>0){
                    echo 'Cod.Fiscale: '.$ordine->obj["shipping_address"]->dni.'<br>';
                  }
                  if(strlen($ordine->obj["shipping_address"]->custom_tipologiaCliente)>0){
                    echo 'Tip. Cliente: '.$ordine->obj["shipping_address"]->custom_tipologiaCliente.'<br>';
                  } 

                  echo '<input type="hidden" name="id_order" value="'.$ordine->id_order.'"/>';

                  echo '<input type="hidden" name="SiglaProvincia" value="'.$validato->siglaProvincia.'"/>';

                  echo '<input type="hidden" name="Cap" value="'.$validato->cap.'"/>';

                  echo '<input type="hidden" name="Localita" value="'.addslashes($validato->comune).'"/>';

                  echo '<input type="hidden" name="Indirizzo" value="'.addslashes($validato->indirizzo).'"/>';



                  
                  echo '<button type="submit" class="btn btn-success">Chiudi Spedizione e Trasmetti a GLS</button>';


                  echo '</form>';
                  
                  echo '</p>';

                }

                }else{


                echo '<p>';
                echo '<form method="POST" action="/gls/addparcel">';
                  echo '<h3>Indirizzo di Spedizione validato in data: '.date('d-m-Y H:i',$validato->creationTime).'</h3>';
                  echo 'Alias: '.$ordine->obj["shipping_address"]->alias.'<br>';
                  if(strlen($ordine->obj["shipping_address"]->company)>0){
                        echo 'Nome Azienda: '.$ordine->obj["shipping_address"]->company.'<br>';    
                  }
                            
                  echo 'Nome e Cognome: '.$ordine->obj["shipping_address"]->firstname.' '.$ordine->obj["shipping_address"]->lastname.'<br>';
                  echo 'Indirizzo: '.$validato->indirizzo.'<br>';
                  echo 'CAP: '.$validato->cap.'<br>';
                  echo 'Città: '.$validato->comune.'<br>';
                  if(strlen($validato->frazione)>0){
                    echo 'Frazione: '.$validato->frazione.'<br>';
                  }
                  echo 'Provincia: '.$validato->descProvincia.' ('.$validato->siglaProvincia.')<br>';
                  if(strlen($ordine->obj["shipping_address"]->phone)>0){
                    echo 'Telefono: '.$ordine->obj["shipping_address"]->phone.'<br>';
                  }
                  if(strlen($ordine->obj["shipping_address"]->phone_mobile)>0){
                    echo 'Cellulare: '.$ordine->obj["shipping_address"]->phone_mobile.'<br>';
                  }
                  if(strlen($ordine->obj["shipping_address"]->vat_number)>0){
                    echo 'P.Iva: '.$ordine->obj["shipping_address"]->vat_number.'<br>';
                  }
                  if(strlen($ordine->obj["shipping_address"]->dni)>0){
                    echo 'Cod.Fiscale: '.$ordine->obj["shipping_address"]->dni.'<br>';
                  }
                  if(strlen($ordine->obj["shipping_address"]->custom_tipologiaCliente)>0){
                    echo 'Tip. Cliente: '.$ordine->obj["shipping_address"]->custom_tipologiaCliente.'<br>';
                  } 

                  echo '<input type="hidden" name="id_order" value="'.$ordine->id_order.'"/>';

                  echo '<input type="hidden" name="SiglaProvincia" value="'.$validato->siglaProvincia.'"/>';

                  echo '<input type="hidden" name="Cap" value="'.$validato->cap.'"/>';

                  echo '<input type="hidden" name="Localita" value="'.addslashes($validato->comune).'"/>';

                  echo '<input type="hidden" name="Indirizzo" value="'.addslashes($validato->indirizzo).'"/>';



                  
                  echo '<button type="submit" class="btn btn-success">Inserisci Spedizione su Portale GLS</button>';


                  echo '</form>';
                  
                  echo '</p>';

                }

              }else{

                if($ordine->current_state==4){

                  $validato = FetchObject(Database()->query("SELECT * FROM `validated_addresses` WHERE id_order = '".$ordine->id_order."' ORDER BY id DESC LIMIT 1"),0);
    
              
                  $inserito = FetchObject(Database()->query("SELECT * FROM `added_parcel` WHERE id_order = '".$ordine->id_order."' ORDER BY id DESC LIMIT 1"),0);
                

                  echo '<p>';
                        echo '<form method="POST" action="/spedizioni/stampa">';
                        echo '<h3>Spedizione trasmessa a GLS in data: '.date('d-m-Y H:i',$trasmesso->creationTime).'</h3>';
                          echo 'Alias: '.$ordine->obj["shipping_address"]->alias.'<br>';
                          
                                    
                          echo 'Nome e Cognome: '.$inserito->DenominazioneDestinatario.'<br>';
                          echo 'Indirizzo: '.$inserito->IndirizzoDestinatario.'<br>';
                          echo 'CAP: '.$validato->cap.'<br>';
                          echo 'Città: '.$validato->comune.'<br>';
                          if(strlen($validato->frazione)>0){
                            echo 'Frazione: '.$validato->frazione.'<br>';
                          }
                          echo 'Provincia: '.$inserito->ProvinciaDestinatario.'<br>';
                          if(strlen($ordine->obj["shipping_address"]->phone)>0){
                            echo 'Telefono: '.$ordine->obj["shipping_address"]->phone.'<br>';
                          }
                          if(strlen($ordine->obj["shipping_address"]->phone_mobile)>0){
                            echo 'Cellulare: '.$ordine->obj["shipping_address"]->phone_mobile.'<br>';
                          }
                          if(strlen($ordine->obj["shipping_address"]->vat_number)>0){
                            echo 'P.Iva: '.$ordine->obj["shipping_address"]->vat_number.'<br>';
                          }
                          if(strlen($ordine->obj["shipping_address"]->dni)>0){
                            echo 'Cod.Fiscale: '.$ordine->obj["shipping_address"]->dni.'<br>';
                          }
                          if(strlen($ordine->obj["shipping_address"]->custom_tipologiaCliente)>0){
                            echo 'Tip. Cliente: '.$ordine->obj["shipping_address"]->custom_tipologiaCliente.'<br>';
                          } 

                          echo '<input type="hidden" name="id_order" value="'.$ordine->id_order.'"/>';

                          

                          
                          echo '<button type="submit" class="btn btn-danger">Stampa etichetta</button>';


                          echo '</form>';
                          
                          echo '</p>';

                }else{
    

                echo '<p>';
                echo '<form method="POST" action="/gls/checkaddress">';
                //echo '<form method="POST" action="/gls/addparcel">';
                echo '<h3>Indirizzo di Spedizione</h3>';
                echo 'Alias: '.$ordine->obj["shipping_address"]->alias.'<br>';
                if(strlen($ordine->obj["shipping_address"]->company)>0){
                      echo 'Nome Azienda: '.$ordine->obj["shipping_address"]->company.'<br>';    
                }
                          
                echo 'Nome e Cognome: '.$ordine->obj["shipping_address"]->firstname.' '.$ordine->obj["shipping_address"]->lastname.'<br>';
                echo 'Indirizzo: '.$ordine->obj["shipping_address"]->address1.' '.$ordine->obj["shipping_address"]->address2.'<br>';
                echo 'CAP: '.$ordine->obj["shipping_address"]->postcode.'<br>';
                echo 'Città: '.$ordine->obj["shipping_address"]->city.'<br>';
                if(strlen($ordine->obj["shipping_address"]->phone)>0){
                  echo 'Telefono: '.$ordine->obj["shipping_address"]->phone.'<br>';
                }
                if(strlen($ordine->obj["shipping_address"]->phone_mobile)>0){
                  echo 'Cellulare: '.$ordine->obj["shipping_address"]->phone_mobile.'<br>';
                }
                if(strlen($ordine->obj["shipping_address"]->vat_number)>0){
                  echo 'P.Iva: '.$ordine->obj["shipping_address"]->vat_number.'<br>';
                }
                if(strlen($ordine->obj["shipping_address"]->dni)>0){
                  echo 'Cod.Fiscale: '.$ordine->obj["shipping_address"]->dni.'<br>';
                }
                if(strlen($ordine->obj["shipping_address"]->custom_tipologiaCliente)>0){
                  echo 'Tip. Cliente: '.$ordine->obj["shipping_address"]->custom_tipologiaCliente.'<br>';
                } 

                echo '<input type="hidden" name="id_order" value="'.$ordine->id_order.'"/>';

                echo '<input type="hidden" name="SiglaProvincia" value="'.$ordine->obj["shipping_address"]->id_state.'"/>';

                echo '<input type="hidden" name="Cap" value="'.$ordine->obj["shipping_address"]->postcode.'"/>';

                echo '<input type="hidden" name="Localita" value="'.$ordine->obj["shipping_address"]->city.'"/>';

                echo '<input type="hidden" name="Indirizzo" value="'.addslashes($ordine->obj["shipping_address"]->address1.' '.$ordine->obj["shipping_address"]->address2).'"/>';



                
                echo '<button type="submit" class="btn btn-success">Valida Indirizzo su stradario GLS</button>';
                //echo '<button type="submit" class="btn btn-success">Test AddParcel GLS</button>';


                echo '</form>';
                
                echo '</p>';

                }

              }

            }else{
              //$validato = LoadApp('spedizioni', 1)->getValidato($ordine->id_order);
             
             if($_GET["esito"]=='ok'){
                $validato = FetchObject(Database()->query("SELECT * FROM `validated_addresses` WHERE id_order = '".$ordine->id_order."' ORDER BY id DESC LIMIT 1"),0);
    
              //echo $validato->creationTime;

            echo '<p>';
            echo '<form method="POST" action="/gls/addparcel">';
              echo '<h3>Indirizzo di Spedizione validato in data: '.date('d-m-Y H:i',$validato->creationTime).'</h3>';
              echo 'Alias: '.$ordine->obj["shipping_address"]->alias.'<br>';
              if(strlen($ordine->obj["shipping_address"]->company)>0){
                    echo 'Nome Azienda: '.$ordine->obj["shipping_address"]->company.'<br>';    
              }
                        
              echo 'Nome e Cognome: '.$ordine->obj["shipping_address"]->firstname.' '.$ordine->obj["shipping_address"]->lastname.'<br>';
              echo 'Indirizzo: '.$validato->indirizzo.'<br>';
              echo 'CAP: '.$validato->cap.'<br>';
              echo 'Città: '.$validato->comune.'<br>';
              if(strlen($validato->frazione)>0){
                echo 'Frazione: '.$validato->frazione.'<br>';
              }
              echo 'Provincia: '.$validato->descProvincia.' ('.$validato->siglaProvincia.')<br>';
              if(strlen($ordine->obj["shipping_address"]->phone)>0){
                echo 'Telefono: '.$ordine->obj["shipping_address"]->phone.'<br>';
              }
              if(strlen($ordine->obj["shipping_address"]->phone_mobile)>0){
                echo 'Cellulare: '.$ordine->obj["shipping_address"]->phone_mobile.'<br>';
              }
              if(strlen($ordine->obj["shipping_address"]->vat_number)>0){
                echo 'P.Iva: '.$ordine->obj["shipping_address"]->vat_number.'<br>';
              }
              if(strlen($ordine->obj["shipping_address"]->dni)>0){
                echo 'Cod.Fiscale: '.$ordine->obj["shipping_address"]->dni.'<br>';
              }
              if(strlen($ordine->obj["shipping_address"]->custom_tipologiaCliente)>0){
                echo 'Tip. Cliente: '.$ordine->obj["shipping_address"]->custom_tipologiaCliente.'<br>';
              } 

              echo '<input type="hidden" name="id_order" value="'.$ordine->id_order.'"/>';

              echo '<input type="hidden" name="SiglaProvincia" value="'.$validato->siglaProvincia.'"/>';

              echo '<input type="hidden" name="Cap" value="'.$validato->cap.'"/>';

              echo '<input type="hidden" name="Localita" value="'.addslashes($validato->comune).'"/>';

              echo '<input type="hidden" name="Indirizzo" value="'.addslashes($validato->indirizzo).'"/>';



              
              echo '<button type="submit" class="btn btn-success">Inserisci Spedizione su Portale GLS</button>';


              echo '</form>';
              
              echo '</p>';
             }else{
              // se èsito == 'ko'
                //echo 'dentro esito = ko<br>';

                $alternative = FetchObject(Database()->query("SELECT * FROM `alternative_addresses` WHERE id_order = '".$ordine->id_order."' ORDER BY id ASC"),1);
                // echo '<pre>';
                // print_r($alternative);
                // echo '</pre>';
                // die();
                
                if(count($alternative)>0){
                  echo '<h2>Indirizzo non identificato. A seguire gli indirizzi più simili.</h2>';
                  echo '<div class="row">';
                  $indice = 0;
                  foreach ($alternative as $singleadress) {
                    echo '
                      <div class="col-xs-6 col-md-4 form-group">
                      <a href="#" class="thumbnail" style="border:0px; text-decoration:none; outline:0px; font-weight:bold; color:black; font-size:14px;">
                        <form method="POST" action="/gls/selectaddress">
                          <input type="hidden" name="id_order" value="'.$ordine->id_order.'"/>
                          
                          <label>Sigla Reg.:</label>
                          <input type="text" class="form-control" name="SiglaRegione" value="'.$singleadress->siglaRegione.'"/>
                          <label>Desc. Reg.:</label>
                          <input type="text" class="form-control" name="DescrizioneRegione" value="'.addslashes($singleadress->descRegione).'"/>
                          <label>Sigla Prov.:</label>
                          <input type="text" class="form-control" name="SiglaProvincia" value="'.$singleadress->siglaProvincia.'"/>
                          <label>Desc. Prov.:</label>
                          <input type="text" class="form-control" name="DescrizioneProvincia" value="'.addslashes($singleadress->descProvincia).'"/>
                          <label>Cap:</label>
                          <input type="text" class="form-control" name="Cap" value="'.$singleadress->cap.'"/>
                          <label>Frazione:</label>
                          <input type="text" class="form-control" name="Frazione" value="'.addslashes($singleadress->frazione).'"/>
                          <label>Comune:</label>
                          <input type="text" class="form-control" name="Comune" value="'.addslashes($singleadress->comune).'"/>
                          <label>Indirizzo:</label>
                          <textarea class="form-control" name="Indirizzo">'.addslashes($singleadress->indirizzo).'</textarea>
                          

                          <input type="hidden" name="InoltroInProvincia" value="'.$singleadress->inoltroInProvincia.'"/>
                          <input type="hidden" name="KmDistanzaSede" value="'.$singleadress->kmDistanzaSede.'"/>
                          <input type="hidden" name="Sede" value="'.$singleadress->sede.'"/>
                          <input type="hidden" name="Zona" value="'.$singleadress->zona.'"/>
                          <input type="hidden" name="TempoDiResa" value="'.$singleadress->tempoDiResa.'"/>
                          <input type="hidden" name="LocalitaDisagiata" value="'.$singleadress->localitaDisagiata.'"/>
                          <input type="hidden" name="ZTL" value="'.$singleadress->ztl.'"/>
                          <input type="hidden" name="Express12" value="'.$singleadress->express12.'"/>
                          <input type="hidden" name="IdentPIN" value="'.$singleadress->identPIN.'"/>
                          <br>
                          <button type="submit" class="btn btn-success">Seleziona questo indirizzo</button>


                        </form>
                      </a>
                      </div>
                    ';
                    $indice++;
                  }
                  echo '</div>';

                  

                }else{
                  echo '<h2>
                  L\'indirizzo sembra non essere presente nel nostro stradario e non vi sono alternative compatibili con l\'indirizzo cercato.<br>
                  Contatta telefonicamente il cliente per correggere l\'indirizzo inserito. 
                  </h2>';
                }
             }

              
            }
            ?>
          </div>

          <div class="jumbotron">
            <?php 
            echo '<p>';
              echo '<h3>Prodotti associati all\'ordine</h3>';
              // echo '<pre>';
              // print_r($ordine->obj["products"]);
              // echo '</pre>';
              ?>

              <div class="table-responsive">
                <table class="table table-striped">
                  <thead>
                    <tr>
                      <th>#Prod.Ref.</th>
                      <th>Nome Prodotto</th>
                      <th>Prez. Unit.</th>
                      <th>Quantità</th>
                      <th>Prez. Tot.</th>
                    </tr>
                  </thead>
                  <tbody>

             <?php  
              foreach ($ordine->obj["products"] as $prodotto) {
                echo '<tr>';
                    echo '<td>'.$prodotto->product_reference.'</td>';
                    echo '<td>'.$prodotto->product_name.'</td>';
                    echo '<td>'.number_format($prodotto->unit_price_tax_incl,2,".","").' &euro;</td>';
                    echo '<td>'.$prodotto->product_quantity.'</td>';
                    echo '<td>'.number_format($prodotto->total_price_tax_incl,2,".","").' &euro;</td>';
                echo '</tr>';
              }
              ?>

                  </tbody>
                </table>
              </div>

            <?php 
            echo '<h4>Totale Ordine: '.number_format($ordine->total_paid,2,".","").' &euro;</h4>';
            echo '</p>';
            ?>
          </div>
          
          
        </div>
      </div>
    </div>

<?php 
}
?>